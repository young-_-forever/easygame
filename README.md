# easygame

#### 介绍
* 轻量级golang微服务服务器

#### 安装教程
```shell
git clone git@gitee.com:young-_-forever/easygame.git
go mod tidy
go run app/main.go
curl http://xxxxx:8000/hello?cmd=say&word=world
```

#### 使用说明
  1. 在``controller``目录新增``hello.controller.go``控制器文件，并加入以写代码
   ```golang
   package controller
   
   import (
   	"gitee.com/young-_-forever/easygame/rpc"
   )
   
   func init() {
   	//新增hello路由
   	rpc.AddService("hello", helloController{}, rpc.NewServiceOptition("user"))
   }
   
   type helloController struct{}
   
   // Say协议入参
   type sayInput struct {
   	// 输入文字 限制5个字符
   	Word string `json:"word" name:"word" valid:"Str" len:"5,5"`
   }
   // Say协议出参
   type sayOutput struct {
   	// 输出hello+word
   	Msg string `json:"msg"`
   }
   
   // say协议
   func (helloController) Say(input sayInput) sayOutput {
   	word := input.Word
   	return sayOutput{
   		Msg: "hello" + word,
   	}
   }
   ```
  2. 打开终端执行``go run app/main.go``
  3. 即可通过``http://xxxxxx:8000/hello?cmd=say&word=world``访问协议