package base

type DataConvertBase interface {
	Decode([]byte) (interface{}, interface{})
	Encode(interface{}, interface{}) []byte
}

// // 下面示例字符串解包打包实现（不可使用，非正式实现）
// type ExampleDataConvert struct {
// }

// func (convert *ExampleDataConvert) Decode(b []byte) (interface{}, interface{}) {
// 	str := *(*string)(unsafe.Pointer(&b))
// 	return 0, str
// }

// func (convert *ExampleDataConvert) Encode(head interface{}, data interface{}) []byte {
// 	str := data.(string)
// 	x := (*[2]uintptr)(unsafe.Pointer(&str))
// 	h := [3]uintptr{x[0], x[1], x[1]}
// 	return *(*[]byte)(unsafe.Pointer(&h))
// }
