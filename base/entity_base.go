package base

import (
	"fmt"
	"strings"
	"time"

	"gitee.com/young-_-forever/easygame/glog"
	"gitee.com/young-_-forever/easygame/gredis"
	"github.com/go-redis/redis"
	"github.com/jmoiron/sqlx"
	uuid "github.com/satori/go.uuid"
)

// 泛型T表示主键数据类型
type EntityBase[T comparable] struct {
	identify    T
	identifyStr string
	components  map[string]ComponentBase[T]
	changed     map[string]bool
	prefix      string
	Db          *sqlx.DB
	Redis       *redis.Client
	rdmVal      string
}

type ComponentBase[T comparable] interface {
	// 获取结构名(一般为数据库表名)
	GetName() string
	// 主键
	IdentifyKey() T
	// 设置主键
	SetIdentifyKey(T)
	// 是否是新数据
	IsNew() bool
	// 设置新数据标记
	SetNew(flag bool)
	// 设置过期时间戳
	SetExpireTick(int64)
	// 获取过期时间戳
	GetExpireTick() int64
	// 获取缓存key
	CacheKey() string
	// 获取数据表名
	GetDbTable() string
	// 获取改变sql数据
	GetChangedSqlData() (string, []interface{})
	// 设置主体实体
	SetEntity(*EntityBase[T])
	// 获取主体实体
	GetEntity() *EntityBase[T]
	// 序列化为字符串
	Marshal() string
	// 反序列化
	Unmarshal(string)
	// 加载函数
	Load() (bool, error)
	// 加载数据库
	LoadDb(*sqlx.DB) (bool, error)
	// 仅保存缓存
	Save(*redis.Client) error
	// 无参数版本
	Saves() error
	// 保存缓存和数据库
	SaveAll(*redis.Client, *sqlx.DB) error
	// 无参数版本(使用默认参数)
	SaveAlls() error
	// 仅保存db
	SaveDb(*sqlx.DB) error
	// 无参数版本
	SaveDbs() error
	SetDirty(bool)
}

func (entity *EntityBase[T]) Init() {
	entity.changed = make(map[string]bool)
	entity.components = make(map[string]ComponentBase[T])
}

func (entity *EntityBase[T]) AllComps() map[string]ComponentBase[T] {
	return entity.components
}

func (entity *EntityBase[T]) AddComponent(component ComponentBase[T]) {
	logger := glog.GetLogger()
	log := logger.Log
	if component == nil {
		log.Error("add component name is nil.")
		return
	}
	name := component.GetName()
	if name == "" {
		log.Error("add component name is empty.", logger.Any("component", component))
		return
	}

	if old, has := entity.components[name]; has {
		log.Error("component name repeated.", logger.Any("old", old), logger.Any("new", component))
	}

	entity.components[name] = component
	component.SetEntity(entity)
}

func (entity *EntityBase[T]) GetComponent(name string) ComponentBase[T] {
	return entity.components[name]
}

func (entity *EntityBase[T]) SetRdmVal(val string) {
	entity.rdmVal = val
}

func (entity *EntityBase[T]) Lock(name string) bool {
	if entity.rdmVal == "" {
		lockVal := uuid.NewV4().String()
		entity.SetRdmVal(lockVal)
	}

	return gredis.Lock(entity.Redis, gredis.CombineKeys(entity.prefix, entity.identifyStr, name), entity.rdmVal)
}

func (entity *EntityBase[T]) Unlock(name string) error {
	return gredis.Unlock(entity.Redis, gredis.CombineKeys(entity.prefix, entity.identifyStr, name), entity.rdmVal)
}

func (entity *EntityBase[T]) IdentifyKey() T {
	return entity.identify
}

func (entity *EntityBase[T]) SetIdentifyKey(id T) {
	entity.identify = id
}

func (entity *EntityBase[T]) SetIdentifyKeyStr(str string) {
	entity.identifyStr = str
}

func (entity *EntityBase[T]) SetPrefix(prefix string) {
	entity.prefix = prefix
}

func (entity *EntityBase[T]) GetPrefix() string {
	if entity.prefix == "" {
		return "data"
	}
	return entity.prefix
}

func (entity *EntityBase[T]) CacheKey() string {
	return fmt.Sprintf("%s_%v", entity.GetPrefix(), entity.IdentifyKey())
}

func (entity *EntityBase[T]) SetChanged(name string) {
	entity.changed[name] = true
}

func (entity *EntityBase[T]) GetChanged() map[string]bool {
	return entity.changed
}

func (entity *EntityBase[T]) Load(client *redis.Client, db *sqlx.DB) error {
	// 没有任何组件不处理
	if len(entity.components) == 0 {
		return nil
	}
	// 先从缓存读
	var hashKeys []string
	for name := range entity.components {
		hashKeys = append(hashKeys, name)
	}

	key := entity.CacheKey()

	datas, err := client.HMGet(key, hashKeys...).Result()
	if err != nil {
		return err
	}

	var needDbKeys []string
	for i := 0; i < len(hashKeys); i++ {
		name := hashKeys[i]
		data := datas[i]
		if data == nil {
			needDbKeys = append(needDbKeys, name)
			continue
		}
		entity.GetComponent(name).Unmarshal(data.(string))
		entity.GetComponent(name).SetNew(false)
	}

	// 从缓存中读不出来的，各自加载db
	for _, name := range needDbKeys {
		entity.GetComponent(name).LoadDb(db)
	}
	// 从db加载完后检查一遍是否需要保存入缓存
	entity.Save(client, true)
	return nil
}

func (entity *EntityBase[T]) Save(client *redis.Client, clear bool) error {
	changedMap := entity.GetChanged()

	var hashKeyMap = make(map[string]interface{})
	for name := range changedMap {
		hashKeyMap[name] = entity.GetComponent(name).Marshal()
	}

	key := entity.CacheKey()
	_, err := client.HMSet(key, hashKeyMap).Result()
	client.Expire(key, time.Second*86400)
	// 保存缓存选择性清理
	if clear {
		entity.Clear()
	}
	return err
}

func (entity *EntityBase[T]) Saves(clear bool) error {
	client := entity.Redis
	return entity.Save(client, clear)
}

func (entity *EntityBase[T]) SaveDb(db *sqlx.DB) (err error) {
	changedMap := entity.GetChanged()
	changedLen := len(changedMap)
	if changedLen == 0 {
		return nil
	}
	if changedLen == 1 {
		for name := range changedMap {
			return entity.GetComponent(name).SaveDb(db)
		}
	}
	// 多个操作使用事务
	tx, err := db.Beginx() // 开启事务
	if err != nil {
		return err
	}
	defer func() {
		if p := recover(); p != nil {
			tx.Rollback()
			switch p := p.(type) {
			case error:
				err = p
			default:
				err = fmt.Errorf("savedb tx err:%v", p)
			}
		} else if err == nil {
			err = tx.Commit()
		}
	}()
	for name := range changedMap {
		c := entity.GetComponent(name)
		sql, params := c.GetChangedSqlData()
		// TODO 以后考虑是否在更新失败时直接插入数据
		res := tx.MustExec(sql, params...)
		log := glog.GetLogger()
		if res == nil {
			log.Log.Warn("save db no sqlresult.", log.String("sql", sql), log.Any("params", params))
		} else {
			rowNum, err := res.RowsAffected()
			if err != nil {
				log.Log.Warn("save db get rowsaffected err.", log.String("sql", sql), log.Any("params", params), log.Any("error", err))
			} else if rowNum == 0 {
				log.Log.Warn("save db no row affected.", log.String("sql", sql), log.Any("params", params))
			}
		}

	}
	// 保存db后必定清理
	entity.Clear()
	return err
}

func (entity *EntityBase[T]) SaveDbs() error {
	db := entity.Db
	return entity.SaveDb(db)
}

func (entity *EntityBase[T]) SaveAll(client *redis.Client, db *sqlx.DB) error {
	err := entity.Save(client, false)
	if err != nil {
		return err
	}
	err = entity.SaveDb(db)
	return err
}

func (entity *EntityBase[T]) SaveAlls() error {
	err := entity.Saves(false)
	if err != nil {
		return err
	}
	err = entity.SaveDbs()
	return err
}

func (entity *EntityBase[T]) Clear() {
	entity.changed = make(map[string]bool)

	for _, comp := range entity.components {
		comp.SetDirty(false)
	}
}

type Component[T comparable] struct {
	identify T
	newFlag  bool
	dirty    bool
	name     string
	cacheKey string
	saveStr  string
	expire   int64
	entity   *EntityBase[T]
}

func (obj *Component[T]) IsNew() bool {
	return obj.newFlag
}

func (obj *Component[T]) SetNew(flag bool) {
	obj.newFlag = flag
}

func (obj *Component[T]) SetName(name string) {
	obj.name = name
}

func (obj *Component[T]) GetName() string {
	return obj.name
}

func (obj *Component[T]) IdentifyKey() T {
	return obj.identify
}

func (obj *Component[T]) SetIdentifyKey(id T) {
	obj.identify = id
}

func (obj *Component[T]) SetEntity(entity *EntityBase[T]) {
	obj.entity = entity
}

func (obj *Component[T]) GetEntity() *EntityBase[T] {
	return obj.entity
}

// 组合结构实现
func (obj *Component[T]) Marshal() string {
	return ""
}

// 组合结构实现
func (obj *Component[T]) Unmarshal(str string) {

}

func (obj *Component[T]) SetCacheKey(cacheKey string) {
	obj.cacheKey = cacheKey
}

func (obj *Component[T]) CacheKey() string {
	if obj.cacheKey == "" {
		obj.SetCacheKey(fmt.Sprintf("data_%v", obj.identify))
	}
	return obj.cacheKey
}

// 组合结构函数补充实现
func (obj *Component[T]) GetDbTable() string {
	return strings.Join([]string{obj.GetName()}, "")
}

func (obj *Component[T]) SetExpireTick(tick int64) {
	obj.expire = tick
}

func (obj *Component[T]) GetExpireTick() int64 {
	return obj.expire
}

// 组合结构函数实现
func (obj *Component[T]) Load() (bool, error) {
	return false, nil
}

// 组合结构函数实现
func (obj *Component[T]) LoadDb(*sqlx.DB) (bool, error) {
	return false, nil
}

// 组合函数实现
func (obj *Component[T]) GetChangedSqlData() (string, []interface{}) {
	return "", nil
}

func (obj *Component[T]) SetSaveStr(str string) {
	obj.saveStr = str
}
func (obj *Component[T]) GetSaveStr() string {
	return obj.saveStr
}

// 保存(单个保存)
func (obj *Component[T]) Save(client *redis.Client) error {
	if !obj.dirty {
		return nil
	}
	if obj.saveStr == "" {
		return nil
	}
	key := obj.CacheKey()
	hmMap := make(map[string]interface{})
	hmMap[obj.GetName()] = obj.saveStr
	err := client.HMSet(key, hmMap).Err()
	if err != nil {
		return err
	}
	obj.saveStr = ""
	// 设置过期
	expireSuccess, err := client.Expire(key, time.Second*time.Duration(obj.GetExpireTick())).Result()
	if err != nil {
		return err
	}
	if !expireSuccess {
		return fmt.Errorf("redis expire key %s failed", key)
	}

	// TODO， 考虑对持久化的一些做法，暂时搁置
	return nil
}

func (obj *Component[T]) Saves() error {
	return obj.Save(obj.GetEntity().Redis)
}

// 组合结构函数实现
func (obj *Component[T]) SaveDb(db *sqlx.DB) error {
	// a := &Component[uint32]{}
	// Test(a)
	return nil
}
func (obj *Component[T]) SaveDbs() error {
	return obj.SaveDb(obj.GetEntity().Db)
}

// 组合结构函数实现，这里是示例写法
func (obj *Component[T]) SaveAll(client *redis.Client, db *sqlx.DB) error {
	err := obj.Save(client)
	if err != nil {
		return err
	}
	err = obj.SaveDb(db)
	return err
}

func (obj *Component[T]) SaveAlls() error {
	return obj.SaveAll(obj.GetEntity().Redis, obj.GetEntity().Db)
}

func (obj *Component[T]) SetDirty(dirty bool) {
	if obj.dirty == dirty {
		return
	}
	obj.dirty = dirty
	if !obj.dirty {
		return
	}
	obj.entity.SetChanged(obj.GetName())
}

// func Test(a ComponentBase[uint32]) {

// }
