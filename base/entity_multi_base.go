package base

import (
	"fmt"
	"strings"
	"time"

	"gitee.com/young-_-forever/easygame/glog"
	"gitee.com/young-_-forever/easygame/utils"
	"github.com/go-redis/redis"
	"github.com/jmoiron/sqlx"
)

type EntityMultiItem[I comparable, T comparable] interface {
	GetIdentify() I
	SetIdentify(I, T)
	Marshal() string
	Unmarshal(string)
	Copy() EntityMultiItem[I, T]
	GetFieldNameStr() string
	GetVals() []interface{}
	GetUpdateNames() []string
	GetUpdateVals() []interface{}
	GetFieldCount() int
	LoadedAfter()
}

type EntityMultiInterface[T comparable, I comparable, D EntityMultiItem[I, T]] interface {
	// 获取结构名(一般为数据库表名)
	GetName() string
	// 关联主键(外键)
	IdentifyKey() T
	// 设置关联主键(外键)
	SetIdentifyKey(T)
	// 设置过期时间戳
	SetExpireTick(int64)
	// 获取过期时间戳
	GetExpireTick() int64
	// 获取缓存key
	CacheKey() string
	// 获取数据表名
	GetDbTable() string
	// 获取改变sql数据
	GetChangedSqlData() ([]string, [][]interface{})
	// 设置缓存
	SetItemCopy(D)
	// 获取缓存拷贝
	GetItemCopy() D
	// 设置项
	SetItem(I, D)
	// 获取项
	GetItem(I) D
	// 加载函数
	Load() (bool, error)
	// 加载数据库
	LoadDb() (bool, error)
	// 仅保存缓存
	Save(*redis.Client) error
	// 无参数版本
	Saves(clear bool) error
	// 保存缓存和数据库
	SaveAll(*redis.Client, *sqlx.DB) error
	// 无参数版本(使用默认参数)
	SaveAlls() error
	// 仅保存db
	SaveDb(*sqlx.DB) error
	// 无参数版本
	SaveDbs() error
}

// 泛型T表示主键数据类型
type EntityMultiBase[T comparable, I comparable, D EntityMultiItem[I, T]] struct {
	identify       T
	prefix         string
	expireTick     int64
	Db             *sqlx.DB
	Redis          *redis.Client
	items          map[I]D
	itemCopy       D
	newItems       []D
	changedItemIds []I
	delItemIds     []I
	delItemIdStrs  []string
	saveStrs       map[string]interface{}
}

func (entity *EntityMultiBase[T, I, D]) Init() {
	entity.items = make(map[I]D)
	entity.newItems = make([]D, 0)
	entity.changedItemIds = make([]I, 0)
	entity.delItemIds = make([]I, 0)
	entity.saveStrs = make(map[string]interface{})
}

func (entity *EntityMultiBase[T, I, D]) IdentifyKey() T {
	return entity.identify
}

func (entity *EntityMultiBase[T, I, D]) SetIdentifyKey(identify T) {
	entity.identify = identify
}

func (entity *EntityMultiBase[T, I, D]) SetPrefix(prefix string) {
	entity.prefix = prefix
}

func (entity *EntityMultiBase[T, I, D]) GetPrefix() string {
	if entity.prefix == "" {
		return "data"
	}
	return entity.prefix
}

// 上层组合补充实现
func (entity *EntityMultiBase[T, I, D]) GetDbTable() string {
	return strings.Join([]string{entity.GetName()}, "")
}

// 如有需要，上层组合实现
func (entity *EntityMultiBase[T, I, D]) GetName() string {
	return entity.prefix
}

func (entity *EntityMultiBase[T, I, D]) CacheKey() string {
	return fmt.Sprintf("%s_%v", entity.GetPrefix(), entity.IdentifyKey())
}

func (entity *EntityMultiBase[T, I, D]) SetChanged(id I) {
	if utils.Includes(entity.changedItemIds, id) {
		return
	}
	entity.changedItemIds = append(entity.changedItemIds, id)
}

func (entity *EntityMultiBase[T, I, D]) SetItemCopy(d D) {
	entity.itemCopy = d
}

func (entity *EntityMultiBase[T, I, D]) GetItemCopy() D {
	return entity.itemCopy.Copy().(D)
}

func (entity *EntityMultiBase[T, I, D]) SetItem(key I, item D) {
	entity.items[key] = item
}

func (entity *EntityMultiBase[T, I, D]) GetItem(key I) D {
	return entity.items[key]
}

func (entity *EntityMultiBase[T, I, D]) GetItemMap() map[I]D {
	return entity.items
}

func (entity *EntityMultiBase[T, I, D]) Add(item D) {
	entity.newItems = append(entity.newItems, item)
	entity.items[item.GetIdentify()] = item
}

func (entity *EntityMultiBase[T, I, D]) Delete(id I) {
	if utils.Includes(entity.delItemIds, id) {
		return
	}
	entity.delItemIds = append(entity.delItemIds, id)
}

func (entity *EntityMultiBase[T, I, D]) SetDelStr(idStr string) {
	if utils.Includes(entity.delItemIdStrs, idStr) {
		return
	}
	entity.delItemIdStrs = append(entity.delItemIdStrs, idStr)
}

func (entity *EntityMultiBase[T, I, D]) GetChangedIds() []I {
	return entity.changedItemIds
}

func (entity *EntityMultiBase[T, I, D]) GetInsertItems() []D {
	return entity.newItems
}

func (entity *EntityMultiBase[T, I, D]) GetDelIds() []I {
	return entity.delItemIds
}

func (entity *EntityMultiBase[T, I, D]) SetSaveStr(key string, val string) {
	entity.saveStrs[key] = val
}

func (entity *EntityMultiBase[T, I, D]) SetExpireTick(tick int64) {
	entity.expireTick = tick
}

func (entity *EntityMultiBase[T, I, D]) GetExpireTick() int64 {
	return entity.expireTick
}

func (entity *EntityMultiBase[T, I, D]) HasChanged() bool {
	return len(entity.changedItemIds) > 0 || len(entity.newItems) > 0 || len(entity.delItemIds) > 0
}

// 上层实现
func (entity *EntityMultiBase[T, I, D]) Load() (bool, error) {
	return false, nil
}

func (entity *EntityMultiBase[T, I, D]) LoadDb() (bool, error) {
	return false, nil
}

func (entity *EntityMultiBase[T, I, D]) GetChangedSqlData() ([]string, [][]interface{}) {
	return []string{}, nil
}

func (entity *EntityMultiBase[T, I, D]) Clear() {
	entity.newItems = make([]D, 0)
	entity.changedItemIds = make([]I, 0)
	entity.delItemIds = make([]I, 0)
}

func (entity *EntityMultiBase[T, I, D]) Save(client *redis.Client, clear bool) error {
	if len(entity.saveStrs) == 0 && len(entity.delItemIdStrs) == 0 {
		return nil
	}
	key := entity.CacheKey()

	if len(entity.saveStrs) > 0 {
		err := client.HMSet(key, entity.saveStrs).Err()
		if err != nil {
			return err
		}
		entity.saveStrs = make(map[string]interface{})
	}

	if len(entity.delItemIdStrs) > 0 {
		client.HDel(key, entity.delItemIdStrs...)
		entity.delItemIdStrs = make([]string, 0)
	}

	// 设置过期
	_, err := client.Expire(key, time.Second*time.Duration(entity.GetExpireTick())).Result()
	if err != nil {
		return err
	}

	return nil
}

func (entity *EntityMultiBase[T, I, D]) Saves(clear bool) error {
	return entity.Save(entity.Redis, clear)
}

// 上层组合实现
func (entity *EntityMultiBase[T, I, D]) SaveDb(db *sqlx.DB, sqls []string, paramsArr [][]interface{}) error {
	if len(sqls) == 0 {
		return nil
	}
	log := glog.GetLogger()
	if len(sqls) != len(paramsArr) {
		log.Log.Error("save db sqls and paramsArr length not equal", log.String("name", entity.GetName()), log.Any("identify", entity.IdentifyKey()))
		return nil
	}
	tx, err := db.Beginx()
	if err != nil {
		return err
	}
	defer func() {
		if p := recover(); p != nil {
			tx.Rollback()
			switch p := p.(type) {
			case error:
				err = p
			default:
				err = fmt.Errorf("multi savedb txerr:%v", p)
			}
		} else if err == nil {
			err = tx.Commit()
		}
	}()
	for i := 0; i < len(sqls); i++ {
		sql := sqls[i]
		params := paramsArr[i]
		res := tx.MustExec(sql, params...)
		log := glog.GetLogger()
		if res == nil {
			log.Log.Warn("multi savedb sql no sqlresult.", log.String("sql", sql), log.Any("params", params))
		} else {
			rowNum, err := res.RowsAffected()
			if err != nil {
				log.Log.Warn("multi savedb sql rowsaffected err.", log.String("sql", sql), log.Any("params", params), log.Any("err", err))
			} else if rowNum == 0 {
				log.Log.Warn("save db no row affected.", log.String("sql", sql), log.Any("params", params))
			}
		}
	}

	entity.Clear()
	return err
}

// 不用
func (entity *EntityMultiBase[T, I, D]) SaveDbs() error {
	return entity.SaveDb(entity.Db, nil, nil)
}

// 不用
func (entity *EntityMultiBase[T, I, D]) SaveAll(client *redis.Client, db *sqlx.DB) error {
	err := entity.Save(client, false)
	if err != nil {
		return err
	}
	return entity.SaveDb(db, nil, nil)
}

func (entity *EntityMultiBase[T, I, D]) SaveAlls() error {
	return entity.SaveAll(entity.Redis, entity.Db)
}

// func Test(a EntityMultiInterface[uint64, uint64]) {

// }

// func Test1() {
// 	o := &EntityMultiBase[uint64, uint64]{}
// 	Test(o)
// }
