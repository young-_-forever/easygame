package base

type PACKET_CMD int32
type BaseMessage struct {
	Cmd  int32  `json:"cmd"`  // 消息号
	Code int32  `code:"code"` // 返回码(0表示成功，其他表示信息)
	Id   uint32 `id:"id"`     // 请求id
}
