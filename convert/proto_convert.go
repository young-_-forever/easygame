package convert

import (
	"encoding/binary"
	"log"
	"runtime"

	"gitee.com/young-_-forever/easygame/base"
	"gitee.com/young-_-forever/easygame/glog"
	"go.uber.org/zap"
	"google.golang.org/protobuf/proto"
)

type CreateMessage func() interface{}
type BaseMsgEncoder func(*base.BaseMessage) ([]byte, error)
type BaseMsgDecoder func([]byte) (*base.BaseMessage, error)

type ProtoDataConvert struct {
	protoMap map[base.PACKET_CMD]CreateMessage
	// 基础消息编码解码处理
	baseMsgEncode BaseMsgEncoder
	baseMsgDecode BaseMsgDecoder
}

func NewProtoDataConvert() *ProtoDataConvert {
	convert := &ProtoDataConvert{}
	convert.protoMap = make(map[base.PACKET_CMD]CreateMessage)
	return convert
}

func (cdc *ProtoDataConvert) RegisterProto(msgId base.PACKET_CMD, f CreateMessage) {
	cdc.protoMap[msgId] = f
}

func (cdc *ProtoDataConvert) SetBaseMsgEncoder(h BaseMsgEncoder) {
	cdc.baseMsgEncode = h
}

func (cdc *ProtoDataConvert) SetBaseMsgDecoder(h BaseMsgDecoder) {
	cdc.baseMsgDecode = h
}

func commonRecover(name string) {
	err := recover()
	if err == nil {
		return
	}
	// TODO 特殊的日志记录方式
	switch err.(type) {
	case runtime.Error: // 运行时错误
		log := glog.GetLogger()
		log.Log.Error("Proto runtime panic ", zap.String("name", name), zap.Any("err", err))
	default: // 非运行错误
		log := glog.GetLogger()
		log.Log.Error("Proto panic ", zap.String("name", name), zap.Any("err", err))
	}
}

func (convert *ProtoDataConvert) Decode(b []byte) (head interface{}, msg interface{}) {
	defer commonRecover("decode")
	if len(b) < 4 {
		log := glog.GetLogger()
		log.Log.Warn("Proto Decode len < 4.")
		return
	}
	if convert.baseMsgDecode == nil || convert.baseMsgEncode == nil {
		log := glog.GetLogger()
		log.Log.Warn("Proto no base create msg func.")
		return
	}

	l := binary.BigEndian.Uint32(b[:4])
	baseBoundary := 4 + l
	if baseBoundary > uint32(len(b)) {
		log := glog.GetLogger()
		log.Log.Warn("Proto base len out of range.")
		return
	}
	baseBytes := b[4:baseBoundary]
	basemsg, err := convert.baseMsgDecode(baseBytes)
	if err != nil {
		glog := glog.GetLogger()
		glog.Log.Error("Proto decode base err.")
		log.Fatal(err)
		return
	}

	head = basemsg
	f, ok := convert.protoMap[base.PACKET_CMD(basemsg.Cmd)]
	if !ok {
		log := glog.GetLogger()
		log.Log.Warn("Proto decode can't find msg func.", log.Int32("cmd", basemsg.Cmd))
		return
	}

	msg = f()

	err = proto.Unmarshal(b[4+l:], msg.(proto.Message))
	if err != nil {
		glog := glog.GetLogger()
		glog.Log.Error("Proto decode packet err.", glog.Int32("cmd", basemsg.Cmd))
		log.Fatal(err)
		return
	}

	return
}

func (convert *ProtoDataConvert) Encode(reqHead *base.BaseMessage, msg interface{}) (b []byte) {
	defer commonRecover("encode")
	baseBytes, err := convert.baseMsgEncode(reqHead)
	if err != nil {
		glog := glog.GetLogger()
		glog.Log.Error("proto encode basemsg err cmd:%d", zap.Any("err", err))
		log.Fatal(err)
		return
	}

	l := make([]byte, 4)
	binary.BigEndian.PutUint32(l, uint32(len(baseBytes)))

	b = append(l, baseBytes...)

	if msg == nil {
		return
	}

	t, err := proto.Marshal(msg.(proto.Message))

	if err != nil {
		glog := glog.GetLogger()
		glog.Log.Error("proto encode msg err", glog.Error("err", err))
		log.Fatal(err)
		return
	}

	b = append(b, t...)
	return
}
