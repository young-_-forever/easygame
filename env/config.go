package env

import "os"

type ConfigBuilder[T comparable] struct {
	// 用于存储配置项的map
	data map[string]T
}

type Config[T comparable] struct {
	// 用于存储配置项的map
	data map[string]T
}

// 创建一个配置项
// devConf: 开发环境配置
// testConf: 测试环境配置
// preConf: 预发布环境配置
// prodConf: 生产环境配置
func NewBuilder[T comparable]() *ConfigBuilder[T] {
	return &ConfigBuilder[T]{
		data: make(map[string]T),
	}
}

// 设置开发环境配置
func (c *ConfigBuilder[T]) WithDev(conf T) *ConfigBuilder[T] {
	c.data[DEVELOPMENT] = conf
	return c
}

// 设置测试环境配置
func (c *ConfigBuilder[T]) WithTest(conf T) *ConfigBuilder[T] {
	c.data[TEST] = conf
	return c
}

// 设置预发布环境配置
func (c *ConfigBuilder[T]) WithPre(conf T) *ConfigBuilder[T] {
	c.data[PREVIEW] = conf
	return c
}

// 设置生产环境配置
func (c *ConfigBuilder[T]) WithProd(conf T) *ConfigBuilder[T] {
	c.data[PRODUCTION] = conf
	return c
}

// 构建配置项
func (c *ConfigBuilder[T]) Build() *Config[T] {
	return &Config[T]{
		data: c.data,
	}
}

// 获取配置项
func (c *Config[T]) Get() T {
	env := os.Getenv("PROJECT_ENV")
	if env == "" {
		env = DEVELOPMENT
	}

	return c.data[env]
}
