package env

import (
	"os"
)

const (
	//开发环境
	DEVELOPMENT = "development"
	//测试环境
	TEST = "test"
	// 预发布环境
	PREVIEW = "preview"
	//正式环境
	PRODUCTION = "production"
)

// 当前环境是否是生产环境
func IsProduction() bool {
	return os.Getenv("PROJECT_ENV") == PRODUCTION
}

// 当前环境是否是开发环境
func IsDevelopment() bool {
	return os.Getenv("PROJECT_ENV") == DEVELOPMENT
}
