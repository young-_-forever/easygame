package gerror

var (
	CTL_NOT_FOUND   = New(10001, "控制器未找到")
	FUNC_NOT_FOUND  = New(10002, "方法未找到")
	NOT_LOGIN_ERR   = New(10003, "未登录")
	UID_EMPTY_ERR   = New(10004, "登陆过期,请您授权登录游戏!")
	SYSTEM_ERR      = New(10005, "出了点小问题~")
	KICK            = New(10006, "在其他地方登录同一账号,被踢下线")
	MYSQL_ERR       = New(10007, "Mysql异常")
	REDIS_ERR       = New(10008, "Redis异常")
	TOO_MANY_TASK   = New(10009, "太多任务排队,稍后再试")
	TASK_OUT_TIME   = New(10010, "任务超时")
	PARAMETER_ERROR = New(10011, "参数错误")
)
