package gerror

type NetError struct {
	Id  int    `json:"id"`
	Msg string `json:"msg"`
}

func New(id int, msg string) *NetError {
	return &NetError{
		id,
		msg,
	}
}
