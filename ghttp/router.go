package ghttp

import (
	"fmt"
	"sync"

	"gitee.com/young-_-forever/easygame/gerror"
	"gitee.com/young-_-forever/easygame/ghttp/task"
)

var (
	queues map[string]*task.TaskQueue = nil
	lock   sync.RWMutex
)

func init() {
	queues = make(map[string]*task.TaskQueue)
}

func getQueueKey(cmd string, proxyId int) string {
	return fmt.Sprintf(cmd+"_%d", proxyId)
}

func getQueue(service *ServiceOptition, proxyId int) *task.TaskQueue {
	queueKey := getQueueKey(service.QueueName, proxyId)
	lock.RLock()
	queue, ok := queues[queueKey]
	lock.RUnlock()
	if !ok {
		return createQueue(queueKey, service.RequestMaxWaitNum)
	}

	return queue
}

func createQueue(queueKey string, maxTaskNum int) *task.TaskQueue {
	lock.Lock()
	queue, ok := queues[queueKey]
	if !ok {
		queue = task.NewTaskQueue(queueKey, maxTaskNum)
		queue.OnClose = func() {
			FreeQueue(queueKey)
		}
		queues[queueKey] = queue
	}

	log.Log.Debug("create queue", log.String("queueKey", queueKey), log.Int("len", len(queues)))
	lock.Unlock()
	return queue
}

func AddToQueue(service *ServiceOptition, proxyId int, task *task.TaskJob) interface{} {
	queue := getQueue(service, proxyId)
	ok := queue.Enqueue(task)
	if !ok {
		return gerror.TOO_MANY_TASK
	}

	return task.GetOutput()
}

func FreeQueue(queueKey string) {
	lock.Lock()
	delete(queues, queueKey)
	lock.Unlock()
	fmt.Println("free queue:", queueKey, "len:", len(queues))
}
