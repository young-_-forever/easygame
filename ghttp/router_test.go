package ghttp

import (
	"testing"
)

func TestProxyKey(t *testing.T) {
	var cmd = "cmd"
	var proxyId = 33335
	var expect = "cmd_33335"
	value := getQueueKey(cmd, proxyId)
	if expect != value {
		t.Errorf("getQueueKey failed.")
	}
}
