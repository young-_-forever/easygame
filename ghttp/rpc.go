package ghttp

import (
	"net/http"
	"strconv"

	"gitee.com/young-_-forever/easygame/ghttp/ip"
	"gitee.com/young-_-forever/easygame/glog"

	"github.com/gin-gonic/gin"
)

var (
	inited chan struct{}
	server *gin.Engine
	log    = glog.GetLogger()
)

func init() {
	inited = make(chan struct{})
}

// init rpc
func Init(log *glog.Logger) {
	server = gin.New()
	server.SetTrustedProxies(nil)
	server.Use(allowCrossdomain())

	close(inited)
}

// Starting the rpc server
func Run(port uint16) {
	<-inited
	for _, s := range serviceMap {
		s.Run(server)
	}
	server.Run(ip.ServerIP + ":" + strconv.FormatUint(uint64(port), 10))
}

func allowCrossdomain() gin.HandlerFunc {
	return func(context *gin.Context) {
		var origin string = context.GetHeader("Access-Control-Allow-Origin")
		if origin == "" {
			origin = "*"
		}

		context.Writer.Header().Set("Access-Control-Allow-Origin", origin)
		context.Header("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE,OPTIONS")
		context.Header("Access-Control-Allow-Headers", "Content-Type,XFILENAME,XFILECATEGORY,XFILESIZE")
		context.Header("Access-Control-Expose-Headers", "Content-Length, Access-Control-Allow-Origin, Access-Control-Allow-Headers,Cache-Control,Content-Language,Content-Type,Expires,Last-Modified,Pragma,FooBar") // 跨域关键设置 让浏览器可以解析
		context.Header("Access-Control-Max-Age", "172800")                                                                                                                                                           // 缓存请求信息 单位为秒
		context.Header("Access-Control-Allow-Credentials", "false")                                                                                                                                                  //  跨域请求是否需要带cookie信息 默认设置为true
		context.Header("content-type", "application/json;charset=utf-8")                                                                                                                                             // 设置返回格式是json
		if context.Request.Method == http.MethodOptions {
			context.JSON(http.StatusOK, "Options Request!")
		}
		context.Next()
	}
}
