package ghttp

import (
	"net/http"
	"reflect"
	"sync"

	"gitee.com/young-_-forever/easygame/ghttp/task"

	"github.com/gin-gonic/gin"
)

var serviceMap map[string]*Service = make(map[string]*Service)
var lockServiceMap sync.Mutex

// 描述服务
type Service struct {
	// name 路由侦听前缀
	name string
	// trflect.valueof(controller)
	rcvr reflect.Value
	// typ := reflect.TypeOf(target)
	typ reflect.Type
	// 控制器中包含的函数
	methods map[string]*MethodType
	//协议额外参数
	optitions *ServiceOptition
}

type ServiceOptition struct {
	// 请求最厂等待时间 /秒 建议5秒
	RequestMaxWaitTime int
	// 协议队列上限 建议20
	RequestMaxWaitNum int
	// 队列名称
	QueueName string
	// 是否开启参数验证 建议开启
	InputValidion bool
}

// 注册服务侦听
// name 路由侦听前缀
// target 路由处理类
// options 额外的参数
func Register(name string, target interface{}, options *ServiceOptition) {
	options = checkServiceOptitions(options)
	createService(name, target, options)
}

// 创建具有默认值的ServiceOptition
func NewServiceOptition(queueName string) *ServiceOptition {
	return &ServiceOptition{
		QueueName:          queueName,
		RequestMaxWaitTime: 5,
		RequestMaxWaitNum:  20,
		InputValidion:      true,
	}
}

// 检查服务配置参数
func checkServiceOptitions(options *ServiceOptition) *ServiceOptition {
	if options == nil {
		options = NewServiceOptition("")
	}

	if options.RequestMaxWaitNum <= 0 {
		panic("The service optitions are incorrectly configured.")
	}

	return options
}

// 创建服务
func createService(name string, target interface{}, options *ServiceOptition) *Service {
	typ := reflect.TypeOf(target)
	methods := GetMethodsMap(typ)
	service := &Service{
		name:      name,
		rcvr:      reflect.ValueOf(target),
		methods:   methods,
		typ:       typ,
		optitions: options,
	}

	lockServiceMap.Lock()
	serviceMap[name] = service
	lockServiceMap.Unlock()

	return service
}

// 路由处理类
type httpRouter struct {
	service *Service
}

// 启动路由侦听
func (s *Service) Run(server *gin.Engine) {
	router := &httpRouter{
		service: s,
	}
	server.Any(s.name, router.Handle)
}

// 协议处理
func (router *httpRouter) Handle(context *gin.Context) {
	args := make(map[string]interface{})
	querys := context.Request.URL.Query()
	for key, value := range querys {
		args[key] = value[0]
	}

	params := context.Params
	for _, param := range params {
		args[param.Key] = param.Value
	}

	//拷贝body附带的参数到map,现阶段仅支持键值对
	context.ShouldBind(&args)

	cmd, ok := args["cmd"]
	if !ok {
		context.JSON(http.StatusBadRequest, struct{}{})
		return
	}

	method, ok := router.service.methods[cmd.(string)]
	if !ok {
		context.JSON(http.StatusBadRequest, struct{}{})
		return
	}
	inputType := method.inputParam

	//创建具象化实例
	input := reflect.Indirect(reflect.New(inputType))
	for i := 0; i < inputType.NumField(); i++ {
		fieldType := inputType.Field(i)
		fieldInfo := input.Field(i)
		if value, ok := args[fieldType.Tag.Get("json")]; ok {
			//给结构体赋值
			//保证赋值时数据类型一致
			if reflect.TypeOf(value) == fieldType.Type {
				fieldInfo.Set(reflect.ValueOf(value))
			}
		}
	}

	//路由入参
	callIn := []reflect.Value{
		router.service.rcvr,
		reflect.ValueOf(input.Interface()),
	}

	log.Log.Debug("request input", log.Any("args", input.Interface()))
	result := router.dispatch(&method.method, &callIn)
	log.Log.Debug("request output", log.Any("result", result))

	context.JSON(http.StatusOK, result)
}

// 协议队列分发
func (router *httpRouter) dispatch(
	method *reflect.Method,
	input *[]reflect.Value,
) interface{} {
	optitions := router.service.optitions
	queueName := optitions.QueueName
	isQueue := queueName != ""

	if !isQueue {
		return method.Func.Call(*input)
	}

	maxWaitTime := optitions.RequestMaxWaitTime

	taskJob := task.NewTaskJob(method, input, maxWaitTime)
	return AddToQueue(optitions, 1, taskJob)
}
