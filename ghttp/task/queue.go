package task

import "time"

type TaskQueue struct {
	queueKey string
	dispatch chan struct{}
	queue    chan *TaskJob
	OnClose  func()
}

func NewTaskQueue(queueKey string, maxLen int) *TaskQueue {
	queue := &TaskQueue{
		queueKey: queueKey,
		queue:    make(chan *TaskJob, maxLen),
		dispatch: make(chan struct{}),
	}

	go queue.start()
	return queue
}

func (queue *TaskQueue) Enqueue(job *TaskJob) bool {
	select {
	case queue.queue <- job:
		return true
	default:
		return false
	}
}

func (queue *TaskQueue) start() {
	isClose := false
	for {
		select {
		case job := <-queue.queue:
			job.Finished()
		case <-time.After(time.Second * 180):
			isClose = true
		}

		if isClose {
			break
		}
	}

	close(queue)
}

func close(queue *TaskQueue) {
	queue.OnClose()
}
