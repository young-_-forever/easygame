package task

import (
	"reflect"
	"time"

	"gitee.com/young-_-forever/easygame/gerror"
)

type TaskJob struct {
	isEnded  bool
	waitTime int
	method   *reflect.Method
	input    *[]reflect.Value
	output   chan []reflect.Value
}

func NewTaskJob(
	method *reflect.Method,
	input *[]reflect.Value,
	waitTime int,
) *TaskJob {
	return &TaskJob{
		method:   method,
		input:    input,
		waitTime: waitTime,
		output:   make(chan []reflect.Value),
	}
}

func (t *TaskJob) GetOutput() interface{} {
	var ret interface{}
	select {
	case <-time.After(time.Second * time.Duration(t.waitTime)):
		ret = gerror.TASK_OUT_TIME
	case result := <-t.output:
		ret = result[0].Interface()
	}

	t.isEnded = true
	return ret
}

func (t *TaskJob) Finished() {
	if !t.isEnded {
		t.output <- t.method.Func.Call(*t.input)
	}
}
