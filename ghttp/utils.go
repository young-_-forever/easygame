package ghttp

import (
	"fmt"
	"reflect"

	"gitee.com/young-_-forever/easygame/utils"
)

// var typeOfError = reflect.TypeOf((*error)(nil)).Elem()

type MethodType struct {
	method     reflect.Method
	inputParam reflect.Type
}

func GetMethodsMap(typ reflect.Type) map[string]*MethodType {
	methods := make(map[string]*MethodType)
	for m := 0; m < typ.NumMethod(); m++ {
		method := typ.Method(m)
		mtype := method.Type
		mname := method.Name
		inputParam := mtype.In(1)

		if mtype.NumOut() != 1 {
			panic(fmt.Sprintf("method %s has wrong number of outs: %v", mname, mtype.NumOut()))
		}

		if mtype.NumOut() != 1 {
			fmt.Printf("method %s is not return value\n", mname)
			continue
		}

		methods[utils.LowerRune(mname, 0)] = &MethodType{method: method, inputParam: inputParam}
	}
	return methods
}
