package glog

import (
	"io"
	"os"
	"path/filepath"
	"sync"
	"time"

	"gitee.com/young-_-forever/easygame/env"
	rotatelogs "github.com/lestrrat-go/file-rotatelogs"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

const (
	outDir   = "./logs/"
	outPath  = "out.log"
	warnPath = "warn.log"
	errPath  = "err.log"
)

type Logger struct {
	Log   *zap.Logger
	Sugar *zap.SugaredLogger
}

func (l *Logger) Init() {
	// log, _ := zap.NewProduction()
	// log, _ := zap.NewDevelopment()

	exePath, err := filepath.Abs(filepath.Dir(os.Args[0]))
	if err != nil {
		panic(err)
	}
	dir := filepath.Join(exePath, outDir)
	_, err = os.Stat(dir)

	if os.IsNotExist(err) {
		err := os.Mkdir(dir, os.ModePerm)
		if err != nil {
			panic(err)
		}
	} else if err != nil {
		panic(err)
	}
	encoder := zapcore.NewConsoleEncoder(zapcore.EncoderConfig{
		MessageKey:    "msg",
		LevelKey:      "level",
		TimeKey:       "ts",
		CallerKey:     "caller",
		StacktraceKey: "trace",
		LineEnding:    zapcore.DefaultLineEnding,
		EncodeLevel:   zapcore.LowercaseLevelEncoder,
		EncodeCaller:  zapcore.ShortCallerEncoder,
		EncodeTime: func(t time.Time, enc zapcore.PrimitiveArrayEncoder) {
			enc.AppendString(t.Format("2006-01-02 15:04:05"))
		},
		EncodeDuration: func(d time.Duration, enc zapcore.PrimitiveArrayEncoder) {
			enc.AppendInt64(int64(d) / 1000000)
		},
	})
	consoleHook := os.Stdout

	if !env.IsProduction() {
		core := zapcore.NewTee(
			zapcore.NewCore(encoder, zapcore.AddSync(consoleHook), zap.DebugLevel),
		)
		l.Log = zap.New(core, zap.AddCaller(), zap.AddStacktrace(zap.WarnLevel))
	} else {
		outHook := getWriter(filepath.Join(dir, "%Y%m%d."+outPath), filepath.Join(dir, outPath))
		warnHook := getWriter(filepath.Join(dir, "%Y%m%d."+warnPath), filepath.Join(dir, warnPath))
		errorHook := getWriter(filepath.Join(dir, "%Y%m%d."+errPath), filepath.Join(dir, errPath))
		core := zapcore.NewTee(
			zapcore.NewCore(encoder, zapcore.AddSync(consoleHook), zap.InfoLevel),
			zapcore.NewCore(encoder, zapcore.AddSync(outHook), zap.InfoLevel),
			zapcore.NewCore(encoder, zapcore.AddSync(warnHook), zap.WarnLevel),
			zapcore.NewCore(encoder, zapcore.AddSync(errorHook), zap.ErrorLevel),
		)

		l.Log = zap.New(core, zap.AddCaller(), zap.AddStacktrace(zap.ErrorLevel))
	}
	l.Sugar = l.Log.Sugar()
	defer l.Log.Sync()
}

func getWriter(fullpath, spath string) io.Writer {
	hook, err := rotatelogs.New(
		fullpath,
		rotatelogs.WithLinkName(spath),
		rotatelogs.WithMaxAge(time.Hour*24*7),
		rotatelogs.WithRotationTime(time.Hour*24),
	)
	if err != nil {
		panic(err)
	}
	return hook
}

func createLogger() *Logger {
	l := &Logger{}
	l.Init()
	return l
}

var log *Logger
var logOnce sync.Once

func GetLogger() *Logger {
	logOnce.Do(func() {
		log = createLogger()
	})
	return log
}

func (l *Logger) Any(key string, val interface{}) zapcore.Field {
	return zap.Any(key, val)
}

func (l *Logger) String(key string, val string) zapcore.Field {
	return zap.String(key, val)
}

func (l *Logger) Int32(key string, val int32) zapcore.Field {
	return zap.Int32(key, val)
}

func (l *Logger) Uint32(key string, val uint32) zapcore.Field {
	return zap.Uint32(key, val)
}

func (l *Logger) Int64(key string, val int64) zapcore.Field {
	return zap.Int64(key, val)
}

func (l *Logger) Uint64(key string, val uint64) zapcore.Field {
	return zap.Uint64(key, val)
}

func (l *Logger) Int(key string, val int) zapcore.Field {
	return zap.Int(key, val)
}

func (l *Logger) Uint8(key string, val uint8) zapcore.Field {
	return zap.Uint8(key, val)
}

func (l *Logger) Error(key string, val error) zapcore.Field {
	return zap.NamedError(key, val)
}
