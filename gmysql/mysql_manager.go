package gmysql

import (
	"fmt"
	"sync"
	"time"

	"gitee.com/young-_-forever/easygame/glog"
	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
)

type MysqlConfig struct {
	Host     string `yaml:"address"`
	Port     uint16 `yaml:"port"`
	Name     string `yaml:"name"`
	User     string `yaml:"user"`
	Password string `yaml:"password"`
}

type MysqlManager struct {
	connector map[*MysqlConfig]*sqlx.DB
	TableDb   *sqlx.DB
}

var mgr *MysqlManager
var once sync.Once
var mx sync.RWMutex

func Instance() *MysqlManager {
	once.Do(func() {
		mgr = &MysqlManager{}
		mgr.connector = make(map[*MysqlConfig]*sqlx.DB)
	})
	return mgr
}

func (mgr *MysqlManager) getConnector(conf *MysqlConfig) *sqlx.DB {
	mx.RLock()
	defer func() {
		mx.RUnlock()
	}()
	connector := mgr.connector[conf]
	return connector
}

func (mgr *MysqlManager) NewConnector(conf *MysqlConfig) (*sqlx.DB, error) {
	db, err := sqlx.Open("mysql", fmt.Sprintf("%s:%s@tcp(%s:%d)/%s?charset=utf8mb4", conf.User, conf.Password, conf.Host, conf.Port, conf.Name))
	if err != nil {
		log := glog.GetLogger()
		log.Log.Error("connect mysql err conf:", log.Any("conf", conf), log.Any("err", err))
		return nil, err
	}
	db.SetMaxOpenConns(20)
	db.SetMaxIdleConns(10)
	db.SetConnMaxLifetime(120 * time.Second)
	db.SetConnMaxIdleTime(120 * time.Second)
	return db, nil
}

func (mgr *MysqlManager) GetConnector(conf *MysqlConfig) *sqlx.DB {
	connector := mgr.getConnector(conf)
	if connector != nil {
		return connector
	}
	mx.Lock()
	defer func() {
		mx.Unlock()
	}()
	// sqlx.BindDriver("mysql", )

	db, _ := mgr.NewConnector(conf)
	if db != nil {
		mgr.connector[conf] = db
	}
	return db
}
