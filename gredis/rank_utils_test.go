package gredis_test

// func TestGetWeekKey(t *testing.T) {
// 	test1 := time.Date(2022, 8, 28, 5, 5, 5, 5, time.Local)
// 	test1Expect := "202208220600"
// 	test1Oupt, _ := redis.GetRankPoint(test1)
// 	if test1Expect != test1Oupt {
// 		t.Error(test1Expect, test1Oupt)
// 	}

// 	test2 := time.Date(2022, 8, 24, 5, 5, 5, 5, time.Local)
// 	test2Expect := "202208220600"
// 	test2Oupt, _ := redis.GetRankPoint(test2)
// 	if test2Expect != test2Oupt {
// 		t.Error(test2Expect, test2Oupt)
// 	}

// 	test3 := time.Date(2022, 8, 24, 6, 00, 00, 00, time.Local)
// 	test3Expect := "202208220600"
// 	test3Oupt, _ := redis.GetRankPoint(test3)
// 	if test3Expect != test3Oupt {
// 		t.Error(test3Expect, test3Oupt)
// 	}

// 	test4 := time.Date(2022, 3, 24, 6, 00, 00, 00, time.Local)
// 	for i := 0; i < 100; i++ {
// 		test3Oupt, _ := redis.GetRankPoint(test4)
// 		tme, _ := time.Parse("200601021504", test3Oupt)
// 		if tme.Weekday() != redis.RANK_RESULT_WEEK_DAY {
// 			t.Error(test3Oupt)
// 		}
// 		test4 = test4.Add(time.Hour * 24 * 7)
// 	}

// 	test5 := time.Date(2022, 8, 22, 6, 00, 00, 00, time.Local)
// 	test5Expect := "202208220600"
// 	test5Oupt, _ := redis.GetRankPoint(test5)
// 	if test5Expect != test5Oupt {
// 		t.Error(test5Expect, test5Oupt)
// 	}
// }
