package gredis

import (
	"strings"
	"sync"
	"time"

	"github.com/go-redis/redis"
)

type RedisConfig struct {
	Host     string `yaml:"host"`
	Port     string `yaml:"port"` //端口号 0-65535
	Password string `yaml:"password"`
	DB       int    `yaml:"db"`
}

// Redis管理类，暂定加连接只在服务器初始化时加入，不然有并发安全问题。
// 如果后续有需求需要运行中多次加client，修改线程安全。
type RedisManager struct {
	connector map[*RedisConfig]*redis.Client // 保证每个配置只有一个client
	singles   []*redis.Client                // 单例Redis存储
	clusters  []*redis.Client                // 集群Redis存储
	marks     []*redis.Client                // 标记管理Redis存储
	limiting  *redis.Client                  // 限流Redis
}

var mgr *RedisManager
var once sync.Once
var getChan chan bool

func Instance() *RedisManager {
	once.Do(func() {
		mgr = &RedisManager{}
		mgr.connector = make(map[*RedisConfig]*redis.Client)
		getChan = make(chan bool, 1)
	})
	return mgr
}

func getChanOver() {
	<-getChan
}

func (mgr *RedisManager) GetRedisClient(conf *RedisConfig) *redis.Client {
	getChan <- true
	defer getChanOver()
	if mgr.connector[conf] != nil {
		return mgr.connector[conf]
	}
	// 不存在时新建
	redisClient := redis.NewClient(&redis.Options{
		Addr:     conf.Host + ":" + conf.Port,
		Password: conf.Password,
		DB:       conf.DB,
		PoolSize: 10,
	})
	mgr.connector[conf] = redisClient
	return redisClient
}

// 设置client限流实例
func (mgr *RedisManager) SetLimitingRedisClient(client *redis.Client) {
	mgr.limiting = client
}

// 设置client到单例数组
func (mgr *RedisManager) SetSingleRedisClient(clients ...*redis.Client) bool {
	mgr.singles = append(mgr.singles, clients...)
	return true
}

// 设置mark到单例数组
func (mgr *RedisManager) SetMarkRedisClient(clients ...*redis.Client) bool {
	mgr.marks = append(mgr.marks, clients...)
	return true
}

// 设置client到集群数组
func (mgr *RedisManager) SetClusterRedisClient(clients ...*redis.Client) bool {
	mgr.clusters = append(mgr.clusters, clients...)
	return true
}

// 获取client限流实例
func (mgr *RedisManager) GetLimitingRedisClient() *redis.Client {
	return mgr.limiting
}

// 获取一个随机单例client
func (mgr *RedisManager) GetRdmSingleRedisClient(id uint32) *redis.Client {
	length := len(mgr.singles)
	if length == 0 {
		return nil
	}
	return mgr.singles[id%uint32(length)]
}

// 获取一个随机标记client
func (mgr *RedisManager) GetRdmMarkRedisClient(id uint64) *redis.Client {
	if len(mgr.marks) == 0 {
		return nil
	}
	return mgr.marks[id%uint64(len(mgr.marks))]
}

// 获取一个随机集群client
func (mgr *RedisManager) GetRdmClustorRedisClient(id uint64) *redis.Client {
	if len(mgr.clusters) == 0 {
		return nil
	}
	return mgr.clusters[id%uint64(len(mgr.clusters))]
}

// 合并键名
func CombineKeys(keys ...string) string {
	return strings.Join(keys, "_")
}

// 加锁
func Lock(client *redis.Client, key, rdmVal string) bool {
	succ, ret := client.SetNX(CombineKeys(key, "lock"), rdmVal, time.Second*5).Result()
	if ret != nil {
		print(ret)
	}
	return succ
}

// 解锁
const unlockScript = `
if redis.call("get", KEYS[1]) == ARGV[1] then
	return redis.call("del", KEYS[1])
else
	return 0
end`

func Unlock(client *redis.Client, key, rdmVal string) error {
	script := redis.NewScript(unlockScript)
	err := script.Run(client, []string{CombineKeys(key, "lock")}, rdmVal).Err()
	return err
}
