package gredis

import (
	"sync"
	"time"

	"gitee.com/young-_-forever/easygame/glog"
	"gitee.com/young-_-forever/easygame/utils"
	"github.com/go-redis/redis"
	uuid "github.com/satori/go.uuid"
)

var log = glog.GetLogger()

const (
	RANK_RESULT_WEEK_DAY   time.Weekday = time.Monday
	TIME_LAYOUT_RANK_POINT string       = "200601021504"
	RANK_MAX               uint32       = 99999999
	BASE_SCORE             float64      = 10000000000
)

// 排行榜结构
type RankEntity[Item comparable] struct {
	Key          string
	Ranks        []Item
	Num          uint32
	Expire       time.Duration // 默认30天
	connector    *redis.Client
	parseFunc    func(key string, score float64) Item
	getWeekPoint func(now time.Time) string
	getDayPoint  func(now time.Time) string
}

// 新建排行
// num 为0代表无限不修剪
func NewRank[Item comparable](key string, num uint32) *RankEntity[Item] {
	rank := &RankEntity[Item]{}
	rank.Num = num
	rank.Key = key
	rank.Expire = 30 * 24 * time.Hour
	return rank
}

func (rank *RankEntity[Item]) SetExpire(expire time.Duration) {
	rank.Expire = expire
}

// 设置周结算时间函数
func (rank *RankEntity[Item]) SetGetWeekPointFunc(f func(now time.Time) (weekTag string, weekPoint time.Time)) {
	rank.getWeekPoint = func(now time.Time) string {
		weekTag, _ := f(now)
		return weekTag
	}
}

// 设置每日结算时间函数
func (rank *RankEntity[Item]) SetDayPointFunc(f func(now time.Time) (dayTag string)) {
	rank.getDayPoint = func(now time.Time) string {
		dayTag := f(now)
		return dayTag
	}
}

// 设置自定义解析函数
func (rank *RankEntity[Item]) SetParseItemFunc(f func(key string, score float64) Item) {
	rank.parseFunc = f
}

// 装载redis
func (rank *RankEntity[Item]) SetRedis(connector *redis.Client) {
	rank.connector = connector
}

// 生成键函数
func (rank *RankEntity[Item]) getTagRedisKey(tags ...string) string {
	return CombineKeys(append([]string{rank.Key}, tags...)...)
}

func (rank *RankEntity[Item]) Load(cnt uint32, tags ...string) bool {
	redisKey := rank.getTagRedisKey(tags...)
	ret, err := rank.connector.ZRevRangeWithScores(redisKey, 0, int64(cnt-1)).Result()
	if err != nil {
		log.Log.Error("rank: Load zrevrangewithscores err", log.String("redisKey", redisKey), log.Any("err", err))
		return false
	}
	rank.Ranks = make([]Item, 0)
	for _, item := range ret {
		rank.Ranks = append(rank.Ranks, rank.parseFunc(item.Member.(string), rank.getScore(item.Score)))
	}
	return true
}

// 加载当前周排行
func (rank *RankEntity[Item]) LoadCurWeek(cnt uint32) bool {
	weekTag := rank.getWeekPoint(time.Now())
	redisKey := rank.getTagRedisKey(weekTag)
	ret, err := rank.connector.ZRevRangeWithScores(redisKey, 0, int64(cnt-1)).Result()
	if err != nil {
		log.Log.Error("rank: LoadWeek zrevrangewithscores err", log.String("redisKey", redisKey), log.Any("err", err))
		return false
	}
	rank.Ranks = make([]Item, 0)
	for _, item := range ret {
		rank.Ranks = append(rank.Ranks, rank.parseFunc(item.Member.(string), rank.getScore(item.Score)))
	}
	return true
}

// 加载上周排行
func (rank *RankEntity[Item]) LoadLastWeek(cnt uint32) bool {
	weekTag := rank.getWeekPoint(time.Now().AddDate(0, 0, -7))
	redisKey := rank.getTagRedisKey(weekTag)
	ret, err := rank.connector.ZRevRangeWithScores(redisKey, 0, int64(cnt-1)).Result()
	if err != nil {
		log.Log.Error("rank: LoadWeek zrevrangewithscores err", log.String("redisKey", redisKey), log.Any("err", err))
		return false
	}
	rank.Ranks = make([]Item, 0)
	for _, item := range ret {
		rank.Ranks = append(rank.Ranks, rank.parseFunc(item.Member.(string), rank.getScore(item.Score)))
	}
	return true
}

func (rank *RankEntity[Item]) LoadDayRank(t time.Time, cnt uint32) bool {
	dayTag := rank.getDayPoint(t)
	redisKey := rank.getTagRedisKey(dayTag)
	ret, err := rank.connector.ZRevRangeWithScores(redisKey, 0, int64(cnt-1)).Result()
	if err != nil {
		log.Log.Error("rank: LoadDayRank zrevrangewithscores err", log.String("redisKey", redisKey), log.Any("err", err))
		return false
	}
	rank.Ranks = make([]Item, 0)
	for _, item := range ret {
		rank.Ranks = append(rank.Ranks, rank.parseFunc(item.Member.(string), rank.getScore(item.Score)))
	}
	return true
}

func (rank *RankEntity[Item]) LoadActDayRank(t time.Time, cnt uint32, actId uint32) bool {
	dayTag := rank.getDayPoint(t)
	redisKey := rank.getTagRedisKey(dayTag, utils.UintToStr(actId))
	ret, err := rank.connector.ZRevRangeWithScores(redisKey, 0, int64(cnt-1)).Result()
	if err != nil {
		log.Log.Error("rank: LoadDayRank zrevrangewithscores err", log.String("redisKey", redisKey), log.Any("err", err))
		return false
	}
	rank.Ranks = make([]Item, 0)
	for _, item := range ret {
		rank.Ranks = append(rank.Ranks, rank.parseFunc(item.Member.(string), rank.getScore(item.Score)))
	}
	return true
}

// 删除玩家排行
func (rank *RankEntity[Item]) Del(fieldKey string) error {
	redisKey := rank.getTagRedisKey()
	_, err := rank.connector.ZRem(redisKey, fieldKey).Result()
	if err != nil && err != redis.Nil {
		log.Log.Error("rank: Del zrem err", log.String("redisKey", redisKey), log.String("fieldKey", fieldKey), log.Any("err", err))
		return err
	}

	return nil
}

// 移除玩家当前周排行
func (rank *RankEntity[Item]) DelCurWeek(fieldKey string) error {
	weekTag := rank.getWeekPoint(time.Now())
	redisKey := rank.getTagRedisKey(weekTag)
	_, err := rank.connector.ZRem(redisKey, fieldKey).Result()
	if err != nil && err != redis.Nil {
		log.Log.Error("rank: DelWeek zrem err", log.String("redisKey", redisKey), log.String("fieldKey", fieldKey), log.Any("err", err))
		return err
	}

	return nil
}

// 异步加载
func (rank *RankEntity[Item]) LoadAsync(wg *sync.WaitGroup, cnt uint32) {
	wg.Add(1)
	go func() {
		rank.Load(cnt)
		wg.Done()
	}()
}

// 获得总榜排行
func (rank *RankEntity[Item]) GetRank(fieldKey string, tags ...string) (uint32, error) {
	redisKey := rank.getTagRedisKey(tags...)
	ret, err := rank.connector.ZRevRank(redisKey, fieldKey).Result()
	if err != nil {
		if err != redis.Nil {
			log.Log.Error("rank: GetRank zrevrank err", log.String("redisKey", redisKey), log.String("fieldKey", fieldKey), log.Any("err", err))
		}
		return RANK_MAX, err
	}

	return uint32(ret) + 1, nil
}

// 获得排行分数
func (rank *RankEntity[Item]) GetScore(fieldKey string, tags ...string) (uint32, error) {
	redisKey := rank.getTagRedisKey(tags...)
	ret, err := rank.connector.ZScore(redisKey, fieldKey).Result()
	if err != nil {
		if err != redis.Nil {
			log.Log.Error("rank: GetScore zscore err", log.String("redisKey", redisKey), log.String("fieldKey", fieldKey), log.Any("err", err))
		}
		return 0, err
	}

	return uint32(rank.getScore(ret)), nil
}

// 获得今日排行分数
func (rank *RankEntity[Item]) GetTodayScore(fieldKey string) (uint32, error) {
	dayTag := rank.getDayPoint(time.Now())
	redisKey := rank.getTagRedisKey(dayTag)
	ret, err := rank.connector.ZScore(redisKey, fieldKey).Result()
	if err != nil {
		if err != redis.Nil {
			log.Log.Error("rank: GetScore zscore err", log.String("redisKey", redisKey), log.String("fieldKey", fieldKey), log.Any("err", err))
		}
		return 0, err
	}

	return uint32(rank.getScore(ret)), nil
}

// 获得活动今日排行分数
func (rank *RankEntity[Item]) GetActTodayScore(fieldKey string, actId uint32) (uint32, error) {
	dayTag := rank.getDayPoint(time.Now())
	redisKey := rank.getTagRedisKey(dayTag, utils.UintToStr(actId))
	ret, err := rank.connector.ZScore(redisKey, fieldKey).Result()
	if err != nil {
		if err != redis.Nil {
			log.Log.Error("rank: GetScore zscore err", log.String("redisKey", redisKey), log.String("fieldKey", fieldKey), log.Any("err", err))
		}
		return 0, err
	}

	return uint32(rank.getScore(ret)), nil
}

// 获得指定周节点排行
// @param weekTag 周排行节点
func (rank *RankEntity[Item]) GetWeekRank(weekTag string, fieldKey string) uint32 {
	redisKey := rank.getTagRedisKey(weekTag)
	ret, err := rank.connector.ZRevRank(redisKey, fieldKey).Result()
	if err != nil {
		if err != redis.Nil {
			log.Log.Error("rank: GetWeekRank zrevrank err", log.String("redisKey", redisKey), log.String("fieldKey", fieldKey), log.Any("err", err))
		}
		return RANK_MAX
	}

	return uint32(ret) + 1
}

// 获得当日玩家排行
func (rank *RankEntity[Item]) GetTodayRank(fieldKey string) (uint32, error) {
	dayTag := rank.getDayPoint(time.Now())
	redisKey := rank.getTagRedisKey(dayTag)
	ret, err := rank.connector.ZRevRank(redisKey, fieldKey).Result()
	if err != nil {
		if err != redis.Nil {
			log.Log.Error("rank: GetTodayRank zrevrank err", log.String("redisKey", redisKey), log.String("fieldKey", fieldKey), log.Any("err", err))
		}
		return RANK_MAX, err
	}

	return uint32(ret) + 1, nil
}

// 获得活动当日玩家排行
func (rank *RankEntity[Item]) GetActTodayRank(fieldKey string, actId uint32) (uint32, error) {
	dayTag := rank.getDayPoint(time.Now())
	redisKey := rank.getTagRedisKey(dayTag, utils.UintToStr(actId))
	ret, err := rank.connector.ZRevRank(redisKey, fieldKey).Result()
	if err != nil {
		if err != redis.Nil {
			log.Log.Error("rank: GetTodayRank zrevrank err", log.String("redisKey", redisKey), log.String("fieldKey", fieldKey), log.Any("err", err))
		}
		return RANK_MAX, err
	}

	return uint32(ret) + 1, nil
}

// 获得活动指定日玩家排行
func (rank *RankEntity[Item]) GetActDayRank(t time.Time, fieldKey string, actId uint32) (uint32, error) {
	dayTag := rank.getDayPoint(t)
	redisKey := rank.getTagRedisKey(dayTag, utils.UintToStr(actId))
	ret, err := rank.connector.ZRevRank(redisKey, fieldKey).Result()
	if err != nil {
		if err != redis.Nil {
			log.Log.Error("rank: GetTodayRank zrevrank err", log.String("redisKey", redisKey), log.String("fieldKey", fieldKey), log.Any("err", err))
		}
		return RANK_MAX, err
	}

	return uint32(ret) + 1, nil
}

// 获得当前周排行
func (rank *RankEntity[Item]) GetCurWeekRank(fieldKey string) (uint32, error) {
	weekTag := rank.getWeekPoint(time.Now())
	redisKey := rank.getTagRedisKey(weekTag)
	ret, err := rank.connector.ZRevRank(redisKey, fieldKey).Result()
	if err != nil {
		if err != redis.Nil {
			log.Log.Error("rank: GetCurWeekRank zrevrank err", log.String("redisKey", redisKey), log.String("fieldKey", fieldKey), log.Any("err", err))
		}
		return RANK_MAX, err
	}

	return uint32(ret) + 1, nil
}

// 获得上周周排行
func (rank *RankEntity[Item]) GetLastWeekRank(fieldKey string) (uint32, error) {
	weekTag := rank.getWeekPoint(time.Now().AddDate(0, 0, -7))
	redisKey := rank.getTagRedisKey(weekTag)
	ret, err := rank.connector.ZRevRank(redisKey, fieldKey).Result()
	if err != nil {
		if err != redis.Nil {
			log.Log.Error("rank: GetCurWeekRank zrevrank err", log.String("redisKey", redisKey), log.String("fieldKey", fieldKey), log.Any("err", err))
		}
		return RANK_MAX, err
	}

	return uint32(ret) + 1, nil
}

func (rank *RankEntity[Item]) getScoreV(score float64) float64 {
	var now = uint64(time.Until(time.Date(2088, 1, 1, 0, 0, 0, 0, time.Local)).Seconds())
	return score*BASE_SCORE + float64(now)
}

func (rank *RankEntity[Item]) getScore(scoreV float64) float64 {
	if scoreV < BASE_SCORE {
		return scoreV
	}

	return float64(scoreV / BASE_SCORE)
}

// 设置玩家排行
func (rank *RankEntity[Item]) Set(fieldKey string, score float64, tags ...string) {
	redisKey := rank.getTagRedisKey(tags...)
	scoreV := rank.getScoreV(score)
	changeNum, err := rank.connector.ZAdd(redisKey, redis.Z{Score: scoreV, Member: fieldKey}).Result()
	if err != nil {
		log.Log.Error("rank: Set zadd err", log.String("redisKey", redisKey), log.String("fieldKey", fieldKey), log.Uint64("scoreV", uint64(scoreV)), log.Any("err", err))
	}

	if changeNum > 0 {
		rank.cutRank(redisKey)
		rank.connector.Expire(redisKey, rank.Expire)
	}
}

// 设置当前天排行
func (rank *RankEntity[Item]) SetCurDay(fieldKey string, score float64) {
	dayTag := rank.getDayPoint(time.Now())
	redisKey := rank.getTagRedisKey(dayTag)
	scoreV := rank.getScoreV(score)
	changeNum, err := rank.connector.ZAdd(redisKey, redis.Z{Score: scoreV, Member: fieldKey}).Result()
	if err != nil {
		log.Log.Error("rank: SetCurDay zadd err", log.String("redisKey", redisKey), log.String("fieldKey", fieldKey), log.Uint64("score", uint64(score)), log.Any("err", err))
		return
	}

	// 对应的key只保存两天
	if changeNum > 0 {
		rank.cutRank(redisKey)
		rank.connector.Expire(redisKey, time.Hour*48)
	}
}

// 天排行分数自增长
func (rank *RankEntity[Item]) IncrDayScore(fieldKey string, add float64) {
	dayTag := rank.getDayPoint(time.Now())
	redisKey := rank.getTagRedisKey(dayTag)
	score, err := rank.connector.ZIncrBy(redisKey, add, fieldKey).Result()
	if err != nil {
		log.Log.Error("rank: AddDayScore zincrby err", log.String("redisKey", redisKey), log.String("fieldKey", fieldKey), log.Uint64("add", uint64(add)), log.Any("err", err))
	}

	if score == add {
		// rank.cutRank(redisKey)
		rank.connector.Expire(redisKey, time.Hour*48)
	}
}

// 活动天排行分数自增长
func (rank *RankEntity[Item]) IncrActDayScore(fieldKey string, add float64, actId uint32) {
	dayTag := rank.getDayPoint(time.Now())
	redisKey := rank.getTagRedisKey(dayTag, utils.UintToStr(actId))
	changeNum, err := rank.connector.ZIncrBy(redisKey, add, fieldKey).Result()
	if err != nil {
		log.Log.Error("rank: AddDayScore zincrby err", log.String("redisKey", redisKey), log.String("fieldKey", fieldKey), log.Uint64("add", uint64(add)), log.Any("err", err))
	}

	if changeNum == add {
		// rank.cutRank(redisKey)
		rank.connector.Expire(redisKey, time.Hour*48)
	}
}

// 分数自增长活动榜
func (rank *RankEntity[Item]) IncrScore(fieldKey string, add float64, actId uint32) {
	redisKey := rank.getTagRedisKey(utils.UintToStr(actId))
	_, err := rank.connector.ZIncrBy(redisKey, add, fieldKey).Result()
	if err != nil {
		log.Log.Error("rank: AddDayScore zincrby err.", log.String("redisKey", redisKey), log.String("fieldKey", fieldKey), log.Uint64("add", uint64(add)), log.Any("err", err))
	}

	rank.connector.Expire(redisKey, time.Hour*24*14)
	// if changeNum == add {
	// 	// rank.cutRank(redisKey)
	// }
}

// 设置玩家当前周排行
func (rank *RankEntity[Item]) SetCurWeek(fieldKey string, score float64) {
	weekTag := rank.getWeekPoint(time.Now())
	redisKey := rank.getTagRedisKey(weekTag)
	scoreV := rank.getScoreV(score)
	changeNum, err := rank.connector.ZAdd(redisKey, redis.Z{Score: scoreV, Member: fieldKey}).Result()
	if err != nil {
		log.Log.Error("rank: SetCurWeek zadd err", log.String("redisKey", redisKey), log.String("fieldKey", fieldKey), log.Uint64("score", uint64(score)), log.Any("err", err))
	}

	if changeNum > 0 {
		rank.cutRank(redisKey)
		rank.connector.Expire(redisKey, time.Hour*24*14)
	}
}

// 修剪排行数据
func (rank *RankEntity[Item]) cutRank(redisKey string) {
	if rank.Num <= 0 {
		return
	}

	totalNum, err := rank.connector.ZCard(redisKey).Result()
	if err != nil {
		log.Log.Warn("rank: SetCurWeek ZCard err", log.String("redisKey", redisKey))
		return
	}
	// 修剪排行量
	if totalNum > int64(rank.Num) {
		rank.connector.ZRemRangeByRank(redisKey, 0, totalNum-int64(rank.Num)-1)
	}
}

func (rank *RankEntity[Item]) getWeekPointLockKey(weekTag string) string {
	return CombineKeys(rank.getTagRedisKey(), weekTag, "lock")
}

func (rank *RankEntity[Item]) getDayPointLockKey(dayTag string) string {
	return CombineKeys(rank.getTagRedisKey(), dayTag, "lock")
}

// 写入周标记节点存储
func (rank *RankEntity[Item]) CheckSaveWeekPoint() error {
	weekTag := rank.getWeekPoint(time.Now().AddDate(0, 0, -7))
	weekRedisKey := rank.getTagRedisKey(weekTag)
	intResult := rank.connector.Exists(weekRedisKey)
	if intResult.Err() != nil {
		log.Log.Error("rank: CheckSaveWeekPoint exists err", log.String("redisKey", weekRedisKey), log.Any("err", intResult.Err()))
		return nil
	}

	if intResult.Val() == 1 {
		return nil
	}

	// 加锁保存节点
	lockKey := rank.getWeekPointLockKey(weekTag)
	lockVal := uuid.NewV4().String()
	isLock := Lock(rank.connector, lockKey, lockVal)
	if !isLock {
		return nil
	}
	defer Unlock(rank.connector, lockKey, lockVal)

	// // 高精度要求时最好一次效验
	// intResult = rank.connector.Exists(weekRedisKey)
	// if intResult.Err() != nil {
	// 	return intResult.Err()
	// }

	// if intResult.Val() == 1 {
	// 	return nil
	// }

	redisKey := rank.getTagRedisKey()
	rankResult := rank.connector.ZRevRangeWithScores(redisKey, 0, 150)
	if rankResult.Err() != nil {
		log.Log.Error("rank: CheckSaveWeekPoint exists err", log.String("redisKey", weekRedisKey), log.Any("err", rankResult.Err()))
		return rankResult.Err()
	}

	_, err := rank.connector.ZAdd(weekRedisKey, rankResult.Val()...).Result()
	if err != nil {
		// 原子操作，失败则删除等待重试
		rank.connector.Del(weekRedisKey)
		return err
	}

	rank.connector.Expire(weekRedisKey, time.Hour*24*28)
	return nil
}

// 写入每日标记节点存储
func (rank *RankEntity[Item]) CheckSaveDayPoint() error {
	dayTag := rank.getDayPoint(time.Now().AddDate(0, 0, -1))
	dayRedisKey := rank.getTagRedisKey(dayTag)
	intResult := rank.connector.Exists(dayRedisKey)
	if intResult.Err() != nil {
		log.Log.Error("rank: CheckSaveDayPoint exists err", log.String("redisKey", dayRedisKey), log.Any("err", intResult.Err()))
		return nil
	}

	if intResult.Val() == 1 {
		return nil
	}

	// 加锁保存节点
	lockKey := rank.getDayPointLockKey(dayTag)
	lockVal := uuid.NewV4().String()
	isLock := Lock(rank.connector, lockKey, lockVal)
	if !isLock {
		return nil
	}
	defer Unlock(rank.connector, lockKey, lockVal)

	// // 高精度要求时最好一次效验
	// intResult = rank.connector.Exists(weekRedisKey)
	// if intResult.Err() != nil {
	// 	return intResult.Err()
	// }

	// if intResult.Val() == 1 {
	// 	return nil
	// }

	redisKey := rank.getTagRedisKey()
	rankResult := rank.connector.ZRevRangeWithScores(redisKey, 0, 100)
	if rankResult.Err() != nil {
		log.Log.Error("rank: CheckSaveDayPoint exists err", log.String("redisKey", dayRedisKey), log.Any("err", rankResult.Err()))
		return rankResult.Err()
	}

	_, err := rank.connector.ZAdd(dayRedisKey, rankResult.Val()...).Result()
	if err != nil {
		// 原子操作，失败则删除等待重试
		rank.connector.Del(dayRedisKey)
		return err
	}

	rank.connector.Expire(dayRedisKey, time.Hour*48)
	return nil
}

// 写入每日标记节点存储
func (rank *RankEntity[Item]) CheckSaveWeekDayPoint() error {
	dayTag := rank.getDayPoint(time.Now().AddDate(0, 0, -1))
	dayRedisKey := rank.getTagRedisKey(dayTag)
	intResult := rank.connector.Exists(dayRedisKey)
	if intResult.Err() != nil {
		log.Log.Error("rank: CheckSaveDayPoint exists err", log.String("redisKey", dayRedisKey), log.Any("err", intResult.Err()))
		return nil
	}

	if intResult.Val() == 1 {
		return nil
	}

	// 加锁保存节点
	lockKey := rank.getDayPointLockKey(dayTag)
	lockVal := uuid.NewV4().String()
	isLock := Lock(rank.connector, lockKey, lockVal)
	if !isLock {
		return nil
	}
	defer Unlock(rank.connector, lockKey, lockVal)

	// // 高精度要求时最好一次效验
	// intResult = rank.connector.Exists(weekRedisKey)
	// if intResult.Err() != nil {
	// 	return intResult.Err()
	// }

	// if intResult.Val() == 1 {
	// 	return nil
	// }
	weekTag := rank.getWeekPoint(time.Now())
	redisKey := rank.getTagRedisKey(weekTag)
	rankResult := rank.connector.ZRevRangeWithScores(redisKey, 0, 100)
	if rankResult.Err() != nil {
		log.Log.Error("rank: CheckSaveDayPoint exists err", log.String("redisKey", dayRedisKey), log.Any("err", rankResult.Err()))
		return rankResult.Err()
	}

	_, err := rank.connector.ZAdd(dayRedisKey, rankResult.Val()...).Result()
	if err != nil {
		// 原子操作，失败则删除等待重试
		rank.connector.Del(dayRedisKey)
		return err
	}

	rank.connector.Expire(dayRedisKey, time.Hour*48)
	return nil
}
