package gtime

import "time"

const FORMATER = "2006-01-02 15:04:05.000"

var (
	offsetDay     int = 0
	offsetHour    int = 0
	offsetMinutes int = 0
	offsetSecond  int = 0
)

// 现在时间戳（毫秒）
func Now() uint64 {
	return uint64(time.Now().UnixMilli()) + uint64(offsetDay*24*3600+offsetHour*3600+offsetMinutes*60+offsetSecond)*1000
}

func Time() time.Time {
	return time.Now().Add(time.Duration(offsetDay*24*3600+offsetHour*3600+offsetMinutes*60+offsetSecond) * time.Second)
}

// 设置时间偏移值
func SetOffset(day, hour, minutes, second int) {
	offsetDay = day
	offsetHour = hour
	offsetMinutes = minutes
	offsetSecond = second
}
