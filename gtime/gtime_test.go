package gtime_test

import (
	"testing"
	"time"
)

func TestNow(t *testing.T) {
	t.Log(time.Now().UnixMilli())
}

func TestTime(t *testing.T) {
	t.Log(time.Now().Add(time.Duration(0*24*3600+0*3600+0*60+0) * time.Second))
}
