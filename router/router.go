package router

import (
	"runtime"

	"gitee.com/young-_-forever/easygame/glog"
	"go.uber.org/zap"
)

type HandlerRet struct {
	Cmd  int32
	Msg  interface{}
	Code int32
}

type MessageHandler func(msg, conn interface{}) *HandlerRet

type Router struct {
	handlerMap       map[int32]MessageHandler
	DefaultNoHandler func(int32, interface{}, interface{})
}

func CreateRouter() *Router {
	router := &Router{}
	router.handlerMap = make(map[int32]MessageHandler)
	router.DefaultNoHandler = defaultNoHandler
	return router
}

func (router *Router) RegisterHandler(cmd int32, handler MessageHandler) {
	router.handlerMap[cmd] = handler
}

func routerRecover(cmd int32, msg interface{}) {
	err := recover()
	if err == nil {
		return
	}
	// TODO 特殊的日志记录方式
	switch err.(type) {
	case runtime.Error: // 运行时错误
		log := glog.GetLogger()
		log.Log.Error("router runtime panic ", zap.Any("err", err), zap.Int32("cmd", cmd), zap.Any("msg", msg))
	default: // 非运行错误
		log := glog.GetLogger()
		log.Log.Error("router panic ", zap.Any("err", err), zap.Int32("cmd", cmd), zap.Any("msg", msg))
	}
}

func defaultNoHandler(cmd int32, msg, conn interface{}) {
	log := glog.GetLogger()
	log.Log.Info("process cmd no handler.", log.Int32("cmd", cmd))
}

func (router *Router) Process(cmd int32, msg, conn interface{}) (int32, interface{}, int32) {
	defer routerRecover(cmd, msg)
	handler, ok := router.handlerMap[cmd]
	if ok {
		ret := handler(msg, conn)
		if ret == nil {
			return 0, nil, 0
		}
		return ret.Cmd, ret.Msg, ret.Code
	}
	if router.DefaultNoHandler == nil {
		log := glog.GetLogger()
		log.Log.Info("process cmd no handler.", log.Int32("cmd", cmd))
	} else {
		router.DefaultNoHandler(cmd, msg, conn)
	}

	return 0, nil, 0
}
