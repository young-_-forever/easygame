package utils

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"encoding/base64"
	"fmt"
	"io"
)

var (
	SECRET_KEY = "d0LOXo9mRAdLdDjG1SDllZShE7gl7E4v"
)

// encrypt 使用AES加密数据，并返回Base64编码的字符串
func Encrypt(bytes []byte) (string, error) {
	keyBytes, err := base64.StdEncoding.DecodeString(SECRET_KEY)
	if err != nil {
		return "", err
	}

	block, err := aes.NewCipher(keyBytes)
	if err != nil {
		return "", err
	}

	ciphertext := make([]byte, aes.BlockSize+len(bytes))
	iv := ciphertext[:aes.BlockSize]
	if _, err := io.ReadFull(rand.Reader, iv); err != nil {
		return "", err
	}

	stream := cipher.NewCFBEncrypter(block, iv)
	stream.XORKeyStream(ciphertext[aes.BlockSize:], bytes)

	return base64.StdEncoding.EncodeToString(ciphertext), nil
}

// decrypt 使用AES解密数据，并返回解密后的字符串
func Decrypt(ciphertext string) ([]byte, error) {
	keyBytes, err := base64.StdEncoding.DecodeString(SECRET_KEY)
	if err != nil {
		return nil, err
	}

	ciphertextBytes, err := base64.StdEncoding.DecodeString(ciphertext)
	if err != nil {
		return nil, err
	}

	block, err := aes.NewCipher(keyBytes)
	if err != nil {
		return nil, err
	}

	if len(ciphertextBytes) < aes.BlockSize {
		return nil, fmt.Errorf("ciphertext is too short")
	}
	iv := ciphertextBytes[:aes.BlockSize]
	ciphertextBytes = ciphertextBytes[aes.BlockSize:]

	stream := cipher.NewCFBDecrypter(block, iv)
	stream.XORKeyStream(ciphertextBytes, ciphertextBytes)

	return ciphertextBytes, nil
}
