package utils

// 从第一个数组中删除第二个数组中有的元素返回新的数组
func ArrDelArr[T comparable](source []T, target []T) []T {
	ret := make([]T, 0)
	m := make(map[T]bool)
	for i := range target {
		m[target[i]] = true
	}
	for i := range source {
		if m[source[i]] {
			continue
		}
		ret = append(ret, source[i])
	}
	return ret
}

// 合并多个数组
func MergeArray[T comparable](arrs ...[]T) []T {
	m := make(map[T]int)

	for _, arr := range arrs {
		for _, v := range arr {
			m[v] = 1
		}
	}

	var r []T

	for k := range m {
		r = append(r, k)
	}

	return r
}

// 判断数组相同
func ArrayEquals[T comparable](arr1, arr2 []T) bool {
	if len(arr1) != len(arr2) {
		return false
	}
	for i := 0; i < len(arr1); i++ {
		if arr1[i] != arr2[i] {
			return false
		}
	}
	return true
}

// (遍历)比较两个Slice内是否所有元素相同
func CompareTwoSlice[T comparable](a []T, b []T) bool {
	if len(a) != len(b) {
		return false
	}
	for i := 0; i < len(a); i++ {
		if a[i] != b[i] {
			return false
		}
	}
	return true
}

// 取多个数的最大值返回
func Max[T Number](nums ...T) T {
	ret := nums[0]
	for _, num := range nums {
		if num > ret {
			ret = num
		}
	}
	return ret
}

// 取多个数的最小值返回
func Min[T Number](nums ...T) T {
	ret := nums[0]
	for _, num := range nums {
		if num < ret {
			ret = num
		}
	}
	return ret
}

// 数字数组求和
func Sum[T Number](nums ...T) T {
	total := T(0)
	for _, num := range nums {
		total += num
	}
	return total
}

// 获取一个元素在不重复数组(切片)的索引
func GetIndex[T comparable](arr []T, v T) int {
	if arr == nil {
		return -1
	}
	for i, v_ := range arr {
		if v_ == v {
			return i
		}
	}
	return -1
}

// 判断数组是否包含元素
func Includes[T comparable](arr []T, v T) bool {
	return GetIndex(arr, v) != -1
}

// 判断数组中某个元素的数量
func NumInArr[T comparable](arr []T, v T) int {
	ret := 0
	for _, v_ := range arr {
		if v == v_ {
			ret++
		}
	}
	return ret
}

// 删除切片内元素
func Del[T comparable](arr []T, v T) ([]T, bool) {
	index := GetIndex(arr, v)
	if index == -1 {
		return arr, false
	}
	for index != -1 {
		arr = DelIndex(arr, index)
		index = GetIndex(arr, v)
	}
	return arr, true
}

// 切片拷贝
func Copy[T comparable](arr []T) []T {
	ret := make([]T, len(arr))
	copy(ret, arr)
	return ret
}

// 删除切片内所有目标元素
func DelAll[T comparable](arr []T, v T) ([]T, bool) {
	succ := false
	arr = Copy(arr)
	for i := len(arr) - 1; i >= 0; i-- {
		if arr[i] == v {
			arr = DelIndex(arr, i)
			succ = true
		}
	}

	return arr, succ
}

func DelIndex[T comparable](arr []T, i int) []T {
	if i >= len(arr) {
		return arr
	}
	arr = append(arr[:i], arr[i+1:]...)
	return arr
}

// 切片去重
func Distinct[T comparable](arr []T) []T {
	var dist []T
	var mp = map[T]bool{}
	for _, item := range arr {
		if _, ok := mp[item]; ok {
			continue
		}

		dist = append(dist, item)
		mp[item] = true
	}

	return dist
}

// 元素重复一定数量到一个数组
func Repeat[T comparable](val T, num int) []T {
	ret := make([]T, num)
	for i := 0; i < num; i++ {
		ret[i] = val
	}
	return ret
}
