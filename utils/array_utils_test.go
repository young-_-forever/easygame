package utils_test

import (
	"reflect"
	"testing"

	"gitee.com/young-_-forever/easygame/utils"
)

func TestDelAll(t *testing.T) {
	type args struct {
		slice []int
	}
	tests := []struct {
		name string
		args args
		want []int
	}{
		{
			name: "TestDelAll1",
			args: args{
				slice: []int{1, 2, 3, 4, 5},
			},
			want: []int{2, 3, 4, 5},
		},
		{
			name: "TestDelAll2",
			args: args{
				slice: []int{1, 1, 3, 4, 1},
			},
			want: []int{3, 4},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got, ok := utils.DelAll(tt.args.slice, 1); !ok || !reflect.DeepEqual(got, tt.want) {
				t.Errorf("DelAll() = %v, want %v", got, tt.want)
			}
		})
	}
}
