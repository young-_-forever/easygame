package binaryutils

// 判断指定id项是否选中
func IsCheck[T int](stateMapPtr map[T]int32, id T) bool {
	var idx T = id / 31
	if stateMapPtr == nil {
		return false
	}

	state, ok := stateMapPtr[idx]
	if !ok {
		return false
	}

	id = id % 31
	return (state & (1 << id)) != 0
}

// 设置指定id项为选中
func SetCheck[T int](stateMapPtr map[T]int32, id T) map[T]int32 {
	idx := id / 31
	if stateMapPtr == nil {
		panic("the state map can not be nil.")
	}

	state := stateMapPtr[idx]
	id = id % 31
	stateMapPtr[idx] = state | (1 << id)
	return stateMapPtr
}

// 取消指定id项为选中
func FreeCheck[T int](stateMapPtr map[T]int32, id T) map[T]int32 {
	idx := id / 31
	if stateMapPtr == nil {
		panic("the state map can not be nil.")
	}

	state := stateMapPtr[idx]
	id = id % 31

	state &= ^(1 << id)
	stateMapPtr[idx] = state
	return stateMapPtr
}

// 获得指定map的所有选中项
func GetCheckIds[T int](stateMapPtr map[T]int32) []T {
	var j T

	idsArr := make([]T, 0, 10)
	stateMap := stateMapPtr
	for idx, state := range stateMap {
		for j = 0; j < 31; j++ {
			tmp := state & (1 << j)
			if tmp > 0 {
				idsArr = append(idsArr, idx*31+j)
			}
		}
	}

	return idsArr
}
