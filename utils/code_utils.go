package utils

import (
	"crypto/md5"
	"crypto/sha256"
	"encoding/hex"
	"fmt"
)

func Md5(source string) string {
	code := fmt.Sprintf("%x", md5.New().Sum([]byte(source)))
	return code
}

func SHA256(key string) string {
	m := sha256.New()
	m.Write(StringToBytes(key))
	res := hex.EncodeToString(m.Sum(nil))
	return res
}
