// 基础计算常用模块
package utils

type UintNumber interface {
	uint | uint8 | uint16 | uint32 | uint64
}

type OnlyIntNumber interface {
	int | int8 | int16 | int32 | int64
}

type UIntAndStr interface {
	UintNumber | string | rune
}

type IntNumber interface {
	UintNumber | OnlyIntNumber
}

type FloatNumber interface {
	float32 | float64
}

type Number interface {
	IntNumber | FloatNumber
}

// 三目表达式
func Ternary[T comparable](a bool, b T, c T) T {
	if a {
		return b
	} else {
		return c
	}
}
