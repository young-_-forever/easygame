package utils

// 合并Map
func MergeMap[K comparable, V any](maps ...map[K]V) map[K]V {
	m := make(map[K]V)
	for _, sourceMap := range maps {
		for key, value := range sourceMap {
			m[key] = value
		}
	}
	return m
}

// 获取map所有键的数组
func Keys[K comparable, V comparable](m map[K]V) []K {
	keys := make([]K, 0, len(m))
	for key := range m {
		keys = append(keys, key)
	}
	return keys
}

// 获取map所有值的数组
func Values[K comparable, V comparable](m map[K]V) []V {
	values := make([]V, 0, len(m))
	for _, value := range m {
		values = append(values, value)
	}
	return values
}

// 通过数组作为键，提供一个默认值作为值生成map
func MapByArray[K comparable, V comparable](keys []K, defaultValue V) map[K]V {
	m := make(map[K]V)
	for _, key := range keys {
		m[key] = defaultValue
	}
	return m
}

// 判断Map相同
func MapEqual[K comparable, V comparable](m1, m2 map[K]V) bool {
	if len(m1) != len(m2) {
		return false
	}
	for k, v := range m1 {
		if v2, ok := m2[k]; !ok || v != v2 {
			return false
		}
	}
	return true
}

// 向map计数器添加计数项
func AddItemToMap[K UIntAndStr, V UintNumber](mp map[K]V, k K, v V) {
	mp[k] = mp[k] + v
}
