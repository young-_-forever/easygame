package utils

import (
	"fmt"
	"math/rand"
	"strconv"
)

// 权重数组随机索引
func RandomByWeight[T UintNumber](weights ...T) uint32 {
	var totalWeight T = 0
	for index := 0; index < len(weights); index++ {
		totalWeight += weights[index]
	}

	randomNumber := Random(0, totalWeight-1)
	var ceilNumber T = 0
	for index := 0; index < len(weights); index++ {
		ceilNumber = ceilNumber + weights[index]
		if randomNumber < ceilNumber && weights[index] > 0 {
			return uint32(index)
		}
	}

	return 0
}

// 打乱
func RandomMixed[T comparable](this []T) {
	n := len(this)
	for i := 0; i < n; i++ {
		index := rand.Intn(n-i) + i
		if index != i {
			this[index], this[i] = this[i], this[index]
		}
	}
}

// 从[min,max]中产生一个数
func RandomFloat64(min, max float64) float64 {
	if min > max {
		return 0
	}
	val := rand.Float64()*(max-min) + min
	f, _ := strconv.ParseFloat(fmt.Sprintf("%.3f", val), 64)
	return f
}

// 从[min,max]中产生一个数
func Random[T IntNumber](min, max T) T {
	if min > max {
		return 0
	}

	return T(rand.Int63n(int64(max-min+1))) + min
}

// 从自定义随机种子中随机一个数
func RandomWithSeed[T IntNumber](seed int64, min, max T) T {
	if min > max {
		return 0
	}

	random := rand.New(rand.NewSource(seed))
	return T(random.Int63n(int64(max-min+1))) + min
}

// 掷硬币
func Coin() bool {
	return Random(0, 1) == 0
}

// 万分比概率判断传入值是否大于随机在0-9999的值
func IsOverTenThousand[T IntNumber](val T) bool {
	return val >= T(rand.Int63n(int64(10000)))
}

// 掷骰子
func Dice() uint32 {
	return Random[uint32](1, 6)
}

// 从一组数中随机选择
// 随机出的数字[min, max]
func RandSelect[T IntNumber](min, max T, num int) []T {
	if min > max {
		return []T{}
	}
	var selected []T

	// 如果总的数量都达不到num，则返回所有的
	if num >= int(max-min+1) {
		for i := min; i <= max; i++ {
			selected = append(selected, i)
		}
		return selected
	}

	var flag = make(map[T]bool)
	var index int = 0
	var length int = int(max-min) + 1

	for index < num {
		v := T(Random(int64(min), int64(max)))
		if _, found := flag[v]; !found {
			selected = append(selected, v)
			flag[v] = true
			index++
		}

		if index >= length {
			break
		}
	}

	return selected
}

// 从一组数中随机选择
// 随机出的数字[min, max]
func RandSelectWithSeed[T IntNumber](min, max T, num int, seed int64) []T {
	if min > max {
		return []T{}
	}
	var selected []T

	// 如果总的数量都达不到num，则返回所有的
	if num >= int(max-min+1) {
		for i := min; i <= max; i++ {
			selected = append(selected, i)
		}
		return selected
	}

	var flag = make(map[T]bool)
	var index int = 0
	var length int = int(max-min) + 1

	for index < num {
		seed += int64(index)
		v := T(RandomWithSeed(seed, int64(min), int64(max)))
		if _, found := flag[v]; !found {
			selected = append(selected, v)
			flag[v] = true
			index++
		}

		if index >= length {
			break
		}
	}

	return selected
}
