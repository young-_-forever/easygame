package snowflake

import (
	"errors"
	"math/rand"
	"sync"
	"time"
)

const (
	epoch        = int64(1609459200000) // 2021-01-01 00:00:00 UTC 的毫秒时间戳
	machineBits  = 10                   // 机器ID位数
	sequenceBits = 12                   // 序列号位数
)

var (
	// 全局默认，随机机器id
	Snowflake *snowflake
)

type snowflake struct {
	mu           sync.Mutex
	machineID    int64
	sequence     int64
	lastStamp    int64
	sequenceMask int64
}

func init() {
	random := rand.New(rand.NewSource(time.Now().UnixNano()))
	machineID := random.Int63n(1<<machineBits - 1)
	sf, err := NewSnowflake(machineID)
	if err != nil {
		panic(err)
	}

	Snowflake = sf
}

func NewSnowflake(machineID int64) (*snowflake, error) {

	sequenceMask := int64(-1) ^ (int64(-1) << sequenceBits)

	return &snowflake{
		machineID:    machineID,
		sequence:     0,
		lastStamp:    -1,
		sequenceMask: sequenceMask,
	}, nil
}

func (s *snowflake) GenerateID() (int64, error) {
	s.mu.Lock()
	defer s.mu.Unlock()

	currentStamp := time.Now().UnixNano() / 1000000 // 转换为毫秒

	if currentStamp < s.lastStamp {
		return 0, errors.New("clock moved backwards")
	}

	if currentStamp == s.lastStamp {
		s.sequence = (s.sequence + 1) & s.sequenceMask
		if s.sequence == 0 {
			// 序列号溢出，等待下一毫秒
			for currentStamp <= s.lastStamp {
				currentStamp = time.Now().UnixNano() / 1000000
			}
		}
	} else {
		s.sequence = 0
	}

	s.lastStamp = currentStamp

	id := ((currentStamp - epoch) << (machineBits + sequenceBits)) | (s.machineID << sequenceBits) | s.sequence
	return id, nil
}
