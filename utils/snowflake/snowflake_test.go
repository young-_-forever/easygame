package snowflake_test

import (
	"testing"

	"gitee.com/young-_-forever/easygame/utils/snowflake"
)

func TestSnowflakeGenerateID(t *testing.T) {
	sf := snowflake.Snowflake

	idSet := make(map[int64]bool)
	numIDs := 100000 // 测试生成多个ID

	for i := 0; i < numIDs; i++ {
		id, err := sf.GenerateID()
		if err != nil {
			t.Errorf("Error generating ID: %v", err)
			return
		}

		// 检查ID是否唯一
		if idSet[id] {
			t.Errorf("Duplicate ID found: %v", id)
			return
		}
		idSet[id] = true
	}
}
