package utils

import (
	"fmt"
	"math/rand"
	"strconv"
	"strings"
	"unsafe"
)

// 把字符串指定位置字母修改为大写形式，仅适用于小数据场景
func UpperRune(str string, idx int) string {
	if str == "" {
		return str
	}

	runeArr := []rune(str)
	if idx >= len(runeArr) {
		return str
	}

	v := runeArr[idx]
	if v < 'a' || v > 'z' {
		return str
	}

	runeArr[idx] -= ('a' - 'A')
	return string(runeArr)
}

// 把字符串指定位置字母修改为大写形式，仅适用于小数据场景
func LowerRune(str string, idx int) string {
	if str == "" {
		return str
	}

	runeArr := []rune(str)
	if idx >= len(runeArr) {
		return str
	}

	v := runeArr[idx]
	if v < 'A' || v > 'Z' {
		return str
	}

	runeArr[idx] += ('a' - 'A')
	return string(runeArr)
}

// 字符串转字节数组(unsafe)
func StringToBytes(str string) []byte {
	x := (*[2]uintptr)(unsafe.Pointer(&str))
	h := [3]uintptr{x[0], x[1], x[1]}
	return *(*[]byte)(unsafe.Pointer(&h))
}

// 字节数组转字符串(unsafe)
func BytesToString(b []byte) string {
	str := *(*string)(unsafe.Pointer(&b))
	return str
}

// 实现字符串占位符
func Format(str string, params ...string) string {
	var sb strings.Builder
	symbol := []byte("{}")
	left := symbol[0]
	right := symbol[1]

	var pos = -1
	for i := 0; i < len(str); i++ {
		if str[i] == left && pos < 0 {
			pos++
		} else if str[i] == right && pos > 0 {
			idx, err := strconv.ParseInt(string(str[i-pos:i]), 10, 32)
			if err != nil {
				panic(err)
			}

			// 假如参数索引位未找到对应字符串时补空格不报错
			if len(params) > int(idx) {
				sb.WriteString(params[idx])
			} else {
				sb.WriteString(" ")
			}
			pos = -1
		} else if str[i] == left && pos >= 0 {
			panic(fmt.Sprintf("formart string error. str: %s idx:%d", str, i))
		} else if str[i] == right && pos <= 0 {
			panic(fmt.Sprintf("formart string error. str: %s idx:%d", str, i))
		} else if pos < 0 {
			sb.WriteByte(str[i])
		} else {
			pos++
		}
	}

	return sb.String()
}

// 字符串补齐0
func StrPadLeft(input string, padLength int) string {
	inputLen := len(input)

	if inputLen >= padLength {
		return input
	}

	needFillLen := padLength - inputLen
	output := strings.Repeat("0", needFillLen)
	return output + input
}

// 数字转字符串Uint
func UintToStr[T UintNumber](value T) string {
	return strconv.FormatUint(uint64(value), 10)
}

// 字符串转数组，忽略多返回值错误
func StrToUint[T UintNumber](str string) T {
	uintNum, _ := strconv.ParseUint(str, 10, 64)
	return T(uintNum)
}

// 数字转字符串int
func IntToStr[T IntNumber](value T) string {
	return strconv.FormatInt(int64(value), 10)
}

// 通用数字数组转字符串数组(大数字有问题？)
func IntsToStrs[T IntNumber](values ...T) []string {
	var res []string
	for _, v := range values {
		res = append(res, strconv.FormatInt(int64(v), 10))
	}
	return res
}

// 随机生成指定长度的字符串
func RandomString(length int) string {
	var sb strings.Builder
	for i := 0; i < length; i++ {
		sb.WriteByte(byte(rand.Intn(26) + 97))
	}
	return sb.String()
}
