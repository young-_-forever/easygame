package utils

import "time"

// 判断两个时间戳/毫秒 是否是同一天
func IsSameDay(t1 uint64, t2 uint64) bool {
	d1 := time.UnixMilli(int64(t1))
	d2 := time.UnixMilli(int64(t2))
	return d1.YearDay() == d2.YearDay() && d1.Year() == d2.Year()
}

// 判断两个时间戳/毫秒 是否是同一天同一小时
func IsSameHour(t1 uint64, t2 uint64) bool {
	d1 := time.UnixMilli(int64(t1))
	d2 := time.UnixMilli(int64(t2))
	return d1.YearDay() == d2.YearDay() && d1.Year() == d2.Year() && d1.Hour() == d2.Hour()
}

// 判断target 是否处于限制内
func IsBetweenInTime(min uint64, target uint64, max uint64) bool {
	// 测试服修改时间
	if max < min {
		return true
	}

	return min <= target && max > target

}

// 每日开始时间戳
func GetDailyStart(nowDate time.Time) uint64 {
	dailyResetStamp := time.Date(
		nowDate.Year(),
		nowDate.Month(),
		nowDate.Day(),
		0,
		0,
		0,
		0,
		time.Local,
	).UnixMilli()
	return uint64(dailyResetStamp)
}

// 计算日期相差多少天
// 返回值day>0, t1>t2; day<0, t1<t2
func SubDays(t1, t2 time.Time) (day int) {
	if t1.Unix() < t2.Unix() {
		temp := t1
		t1 = t2
		t2 = temp
	}

	day = int(t1.Sub(t2).Hours() / 24)
	return
}

// 获得当前周起始点
func GetWeekPoint(
	now time.Time,
	pointWeekDay time.Weekday,
	pointHour int,
	tagLayout string,
) (
	pointTime time.Time,
) {
	todayPoint := time.Date(now.Year(), now.Month(), now.Day(), pointHour, 0, 0, 0, time.Local)
	weekToday := now.Weekday()
	var diffDay time.Weekday = 0
	if weekToday < pointWeekDay {
		diffDay = 7 - pointWeekDay + weekToday
	} else if weekToday == pointWeekDay && now.Hour() < pointHour {
		diffDay = 7 - pointWeekDay + weekToday
	} else {
		diffDay = weekToday - pointWeekDay
	}

	pointTime = todayPoint.AddDate(0, 0, int(diffDay)*-1)
	return
}
