package ws

import (
	"context"
	"fmt"
	"net/http"
	"sync"
	"time"

	"gitee.com/young-_-forever/easygame/base"
	"gitee.com/young-_-forever/easygame/convert"
	"gitee.com/young-_-forever/easygame/env"
	"gitee.com/young-_-forever/easygame/glog"
	"gitee.com/young-_-forever/easygame/router"
	"github.com/gin-contrib/pprof"
	"github.com/gin-gonic/gin"
	"github.com/gorilla/websocket"
	uuid "github.com/satori/go.uuid"
)

// websocket服务管理结构
type WebSocketServer struct {
	context *gin.Engine
	service *gin.Engine
	// Zone                   map[string]map[string]*WebSocketClient
	all              map[string]*WebSocketClient
	certificationAll map[uint64]string
	// zoneCount            uint
	clientCount                         uint
	Lock                                sync.Mutex // 暂时弃用
	router                              *router.Router
	converter                           *convert.ProtoDataConvert
	CertificationHas                    chan *CertificationHasInfo
	Register, UnRegister, Certification chan *WebSocketClient
	SendMsgs                            chan *MessageData
	RecvMsgs                            chan *RecvMessageData
	// ZoneMessage                         chan *ZoneMessageData
	BroadCastMessage  chan *BroadCastMessageData
	initOverFuncs     []func()
	log               *glog.Logger
	beforeMiddlewares []func(base *base.BaseMessage, msg interface{}, c *WebSocketClient) bool
	afterMiddlewares  []func(base *base.BaseMessage, msg interface{}, c *WebSocketClient)
}

type CertificationHasInfo struct {
	Id      uint64
	Fun     func(bool)
	HasChan chan bool
}

// messageData 单个发送数据信息
type MessageData struct {
	Id      uint64
	Message []byte
	Fun     func(bool)
	HasChan chan bool
}

type RecvMessageData struct {
	Id      uint64
	Base    *base.BaseMessage
	Msg     interface{}
	Fun     func(bool)
	HasChan chan bool
}

// zoneMessageData 组广播数据信息
type ZoneMessageData struct {
	Zone    string
	Message []byte
}

// 广播发送数据信息
type BroadCastMessageData struct {
	Message []byte
}

func Cors() gin.HandlerFunc {
	return func(c *gin.Context) {
		method := c.Request.Method

		c.Header("Access-Control-Allow-Origin", "*")
		c.Header("Access-Control-Allow-Headers", "Content-Type,AccessToken,X-CSRF-Token, Authorization, Token")
		c.Header("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE,UPDATE") //服务器支持的所有跨域请求的方
		c.Header("Access-Control-Expose-Headers", "Content-Length, Access-Control-Allow-Origin, Access-Control-Allow-Headers, Content-Type")
		c.Header("Access-Control-Allow-Credentials", "true")

		//放行所有OPTIONS方法
		if method == "OPTIONS" {
			c.AbortWithStatus(http.StatusNoContent)
		}
		// 处理请求
		c.Next()
	}
}

// 初始化 websocket
func (srv *WebSocketServer) Init(port uint16) {

	// 设置路由
	srv.router = router.CreateRouter()
	// 设置解包打包
	srv.converter = convert.NewProtoDataConvert()

	// 日志
	srv.log = glog.GetLogger()
	// 设置ws服务
	gin.SetMode(gin.ReleaseMode)

	srv.context = gin.Default()
	srv.context.Use(Cors())
	srv.context.GET("/", WebSocketServerIns.WsClient)
	srv.context.GET("/health_check", func(ctx *gin.Context) { ctx.String(200, "") })
	srv.context.GET("/color", WebSocketServerIns.WsClient)

	// 非生产环境设置pprof
	if !env.IsProduction() {
		pprof.Register(srv.context)
	}

	srv.service = gin.Default()
	srv.service.Use(Cors())

	for _, fun := range srv.initOverFuncs {
		fun()
	}
	srv.initOverFuncs = []func(){}
	go srv.Start()
	srv.context.Run(fmt.Sprintf("0.0.0.0:%d", port))
	srv.service.Run(fmt.Sprintf("0.0.0.0:%d", port+1))
}

// 仅允许在服务器初始化时调用
func (srv *WebSocketServer) AddBeforeMiddleware(fun func(base *base.BaseMessage, msg interface{}, c *WebSocketClient) bool) {
	srv.beforeMiddlewares = append(srv.beforeMiddlewares, fun)
}

// 仅允许在服务器初始化时调用
func (srv *WebSocketServer) AddAfterMiddleware(fun func(base *base.BaseMessage, msg interface{}, c *WebSocketClient)) {
	srv.afterMiddlewares = append(srv.afterMiddlewares, fun)
}

func (srv *WebSocketServer) GetGinContext() *gin.Engine {
	return srv.context
}

func (srv *WebSocketServer) GetServiceContext() *gin.Engine {
	return srv.service
}

// 启动服务，进行各种绑定
func (srv *WebSocketServer) Start() {
	srv.log.Log.Info("websocket server start")
	for {
		select {
		// 注册
		case client := <-srv.Register:
			srv.log.Log.Info("client connect", srv.log.String("Id", client.Id))
			// srv.log.Sugar.Infof("register client [%s] to zone [%s]", client.Id, client.Zone)
			// srv.log.Sugar.Infof("register client [%s] to zone", client.Id)

			// srv.Lock.Lock()
			if srv.all == nil {
				srv.all = make(map[string]*WebSocketClient)
			}
			if srv.certificationAll == nil {
				srv.certificationAll = make(map[uint64]string)
			}
			// if srv.Zone[client.Zone] == nil {
			// 	srv.Zone[client.Zone] = make(map[string]*WebSocketClient)
			// 	srv.zoneCount += 1
			// }
			// srv.Zone[client.Zone][client.Id] = client
			srv.all[client.Id] = client
			srv.clientCount += 1
			// srv.Lock.Unlock()
		// 认证
		case client := <-srv.Certification:
			srv.log.Log.Info("certification client ", srv.log.String("Id", client.Id), srv.log.Uint64("uid", client.Uid))
			if _, ok := srv.all[client.Id]; ok {
				if id, has := srv.certificationAll[client.Uid]; has {
					srv.all[id].Close()
				}
				srv.certificationAll[client.Uid] = client.Id
			}
		// 注销
		case client := <-srv.UnRegister:
			srv.log.Log.Info("unregister client ", srv.log.String("Id", client.Id), srv.log.Uint64("uid", client.Uid))
			// srv.Lock.Lock()
			if _, ok := srv.all[client.Id]; ok {
				close(client.Message)
				close(client.Recvs)
				delete(srv.all, client.Id)
				if id, has := srv.certificationAll[client.Uid]; has {
					if id == client.Id {
						delete(srv.certificationAll, client.Uid)
					}
				}

				srv.clientCount -= 1
			}
		// srv.log.Sugar.Infof("unregister client [%s] from zone [%s]", client.Id, client.Zone)
		// if _, ok := srv.Zone[client.Zone]; ok {
		// 	if _, ok := srv.Zone[client.Zone][client.Id]; ok {
		// 		close(client.Message)
		// 		close(client.Recvs)
		// 		delete(srv.Zone[client.Zone], client.Id)
		// 		delete(srv.All, client.Id)
		// 		srv.clientCount -= 1
		// 		if len(srv.Zone[client.Zone]) == 0 {
		// 			delete(srv.Zone, client.Zone)
		// 			srv.zoneCount -= 1
		// 		}
		// 	}
		// }
		// srv.Lock.Unlock()
		case cHasInfo := <-srv.CertificationHas:
			_, has := srv.certificationAll[cHasInfo.Id]
			cHasInfo.Fun(has)
		case msg := <-srv.SendMsgs:
			has := false
			if str, ok := srv.certificationAll[msg.Id]; !ok {
				srv.log.Log.Debug("send msg droped ,srv don't has Id", srv.log.Uint64("id", msg.Id))
			} else if conn, ok := srv.all[str]; ok {
				conn.Send(msg.Message)
				has = true
			} else {
				srv.log.Log.Debug("send msg droped ,srv don't has Id", srv.log.Uint64("id", msg.Id))
			}
			if msg.Fun != nil {
				msg.Fun(has)
			}
			if msg.HasChan != nil {
				msg.HasChan <- has
			}
		case msg := <-srv.RecvMsgs:
			has := false
			if str, ok := srv.certificationAll[msg.Id]; !ok {
				srv.log.Log.Debug("recv msg droped ,srv don't has Id", srv.log.Uint64("id", msg.Id))
			} else if conn, ok := srv.all[str]; ok {
				// base, msg := conn.Decode(msg.Message)
				conn.Recv(msg.Base, msg.Msg)
				has = true
			} else {
				srv.log.Log.Debug("revv msg droped ,srv don't has Id", srv.log.Uint64("id", msg.Id))
			}
			if msg.Fun != nil {
				msg.Fun(has)
			}
			if msg.HasChan != nil {
				msg.HasChan <- has
			}
		}

	}

}

// 添加初始化结束事件函数
func (srv *WebSocketServer) AddInitOverFunc(fun func()) {
	srv.initOverFuncs = append(srv.initOverFuncs, fun)
}

func (srv *WebSocketServer) GetRouter() *router.Router {
	return srv.router
}

func (srv *WebSocketServer) GetConverter() *convert.ProtoDataConvert {
	return srv.converter
}

// 处理单个 client 发送数据
// func (srv *WebSocketServer) SendService() {
// 	for data := range srv.SendMsgs {
// 		if conn, ok := srv.all[data.Id]; ok {
// 			conn.Send(data.Message)
// 		}
// 	}
// }

// 处理 zone 广播数据
// func (srv *WebSocketServer) SendZoneService() {
// 	// 发送广播数据到某个组的 channel 变量 Send 中
// 	for data := range srv.ZoneMessage {
// 		if zoneMap, ok := srv.Zone[data.Zone]; ok {
// 			for _, conn := range zoneMap {
// 				conn.Send(data.Message)
// 			}
// 		}
// 	}
// }

// 处理广播数据
// func (srv *WebSocketServer) SendAllService() {
// 	for data := range srv.BroadCastMessage {
// 		for _, v := range srv.Zone {
// 			for _, conn := range v {
// 				conn.Send(data.Message)
// 			}
// 		}
// 	}
// }

// 向指定的 client 发送数据
func (srv *WebSocketServer) SendWithChan(id uint64, message []byte, hasChan chan bool) {
	data := &MessageData{
		Id:      id,
		Message: message,
		HasChan: hasChan,
	}
	srv.SendMsgs <- data
}

// 向指定的 client 发送数据
func (srv *WebSocketServer) SendWithFunc(id uint64, message []byte, fun func(bool)) {
	data := &MessageData{
		Id:      id,
		Message: message,
		Fun:     fun,
	}
	srv.SendMsgs <- data
}

// 向指定的 client 发送数据
func (srv *WebSocketServer) Send(id uint64, message []byte) {
	data := &MessageData{
		Id:      id,
		Message: message,
	}
	srv.SendMsgs <- data
}

// 让指定的 client 接收数据
func (srv *WebSocketServer) RecvWithChan(id uint64, base *base.BaseMessage, message interface{}, hasChan chan bool) {
	data := &RecvMessageData{
		Id:      id,
		Base:    base,
		Msg:     message,
		HasChan: hasChan,
	}
	srv.RecvMsgs <- data
}

// 让指定的 client 接收数据
func (srv *WebSocketServer) RecvWithFunc(id uint64, base *base.BaseMessage, message interface{}, fun func(bool)) {
	data := &RecvMessageData{
		Id:   id,
		Base: base,
		Msg:  message,
		Fun:  fun,
	}
	srv.RecvMsgs <- data
}

// 让指定的 client 接收数据
func (srv *WebSocketServer) Recv(id uint64, base *base.BaseMessage, message interface{}) {
	data := &RecvMessageData{
		Id:   id,
		Base: base,
		Msg:  message,
	}
	srv.RecvMsgs <- data
}

// 向指定的 Zone 广播
// func (srv *WebSocketServer) SendZone(zone string, message []byte) {
// 	data := &ZoneMessageData{
// 		Zone:    zone,
// 		Message: message,
// 	}
// 	srv.ZoneMessage <- data
// }

// 广播
// func (srv *WebSocketServer) SendAll(message []byte) {
// 	data := &BroadCastMessageData{
// 		Message: message,
// 	}
// 	srv.BroadCastMessage <- data
// }

// 注册
func (srv *WebSocketServer) RegisterClient(client *WebSocketClient) {
	srv.Register <- client
}

// 注销
func (srv *WebSocketServer) UnRegisterClient(client *WebSocketClient) {
	srv.UnRegister <- client
}

// 当前组个数
// func (srv *WebSocketServer) LenZone() uint {
// 	return srv.zoneCount
// }

// 当前连接个数
func (srv *WebSocketServer) LenClient() uint {
	return srv.clientCount
}

// 获取 wsManager 管理器信息
func (srv *WebSocketServer) Info() map[string]interface{} {
	srvInfo := make(map[string]interface{})
	// srvInfo["zoneLen"] = srv.LenZone()
	srvInfo["clientLen"] = srv.LenClient()
	srvInfo["chanRegisterLen"] = len(srv.Register)
	srvInfo["chanUnregisterLen"] = len(srv.UnRegister)
	srvInfo["chanMessageLen"] = len(srv.SendMsgs)
	// srvInfo["chanZoneMessageLen"] = len(srv.ZoneMessage)
	srvInfo["chanBroadCastMessageLen"] = len(srv.BroadCastMessage)
	return srvInfo
}

// 初始化服务
var WebSocketServerIns = &WebSocketServer{
	// Zone:             make(map[string]map[string]*WebSocketClient),
	Register:         make(chan *WebSocketClient, 128),
	UnRegister:       make(chan *WebSocketClient, 128),
	Certification:    make(chan *WebSocketClient, 128),
	CertificationHas: make(chan *CertificationHasInfo, 128),
	// ZoneMessage:      make(chan *ZoneMessageData, 128),
	SendMsgs:         make(chan *MessageData, 128),
	RecvMsgs:         make(chan *RecvMessageData, 128),
	BroadCastMessage: make(chan *BroadCastMessageData, 128),
	// zoneCount:        0,
	clientCount: 0,
}

// gin处理websocket handler
func (srv *WebSocketServer) WsClient(ctx *gin.Context) {
	upGrader := websocket.Upgrader{
		// cross origin domain
		CheckOrigin: func(r *http.Request) bool {
			return true
		},
		// 处理 Sec-WebSocket-Protocol Header
		Subprotocols: []string{ctx.GetHeader("Sec-WebSocket-Protocol")},
	}

	conn, err := upGrader.Upgrade(ctx.Writer, ctx.Request, nil)
	if err != nil {
		glog.GetLogger().Log.Info(err.Error())
		srv.log.Sugar.Infof("websocket connect error: %v", err)
		return
	}

	client := &WebSocketClient{
		Id:        uuid.NewV4().String(),
		Socket:    conn,
		Message:   make(chan []byte, 1024),
		Recvs:     make(chan *ClientMsg, 256),
		router:    srv.GetRouter(),
		converter: srv.GetConverter(),
		log:       srv.log,
	}
	client.beforeMiddlewares = srv.beforeMiddlewares
	client.afterMiddlewares = srv.afterMiddlewares

	srv.RegisterClient(client)
	client.StartProcess()
	time.Sleep(time.Second * 15)
}

// TODO 关闭处理
func (src *WebSocketServer) ShutDown(ctx context.Context) error {
	ctx.Done()
	return nil
}
