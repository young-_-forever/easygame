package ws

import (
	"time"

	"gitee.com/young-_-forever/easygame/base"
	"gitee.com/young-_-forever/easygame/convert"
	"gitee.com/young-_-forever/easygame/glog"
	"gitee.com/young-_-forever/easygame/router"
	"github.com/gorilla/websocket"
)

type ClientMsg struct {
	Base *base.BaseMessage
	Msg  interface{}
}

// WebSocketClient单个连接(玩家)websocket信息
type WebSocketClient struct {
	Id string
	// Zone              string
	Socket            *websocket.Conn
	Message           chan []byte
	Recvs             chan *ClientMsg
	router            *router.Router
	converter         *convert.ProtoDataConvert
	log               *glog.Logger
	Uid               uint64
	closed            bool
	closeHandlers     []func()
	beforeMiddlewares []func(base *base.BaseMessage, msg interface{}, c *WebSocketClient) bool
	afterMiddlewares  []func(base *base.BaseMessage, msg interface{}, c *WebSocketClient)
}

func (c *WebSocketClient) AddCloseHandler(f func()) {
	c.closeHandlers = append(c.closeHandlers, f)
}

func (c *WebSocketClient) Close() {
	time.Sleep(time.Millisecond * 50)
	if c.closed {
		return
	}
	c.closed = true
	WebSocketServerIns.UnRegisterClient(c)
	c.log.Sugar.Infof("client [%s] disconnect", c.Id)
	for _, f := range c.closeHandlers {
		f()
	}

	// // 临时等待全部包处理完逻辑
	// start := time.Now()
	// for len(c.Recvs) != 0 || len(c.Message) != 0 {
	// 	time.Sleep(time.Millisecond * 10)
	// 	if time.Since(start) > time.Minute {
	// 		break
	// 	}
	// }
	c.closeHandlers = nil
	c.beforeMiddlewares = nil
	c.afterMiddlewares = nil

	if err := c.Socket.Close(); err != nil {
		c.log.Sugar.Infof("client [%s] disconnect err: %s", c.Id, err)
	}
	// runtime.GC()
}

func (c *WebSocketClient) Decode(message []byte) (*base.BaseMessage, interface{}) {
	head, msg := c.converter.Decode(message)
	base, headSucc := head.(*base.BaseMessage)

	if !headSucc || base == nil || msg == nil {
		c.log.Log.Info("decode head is nil or msg nil.", c.log.Any("head", head), c.log.Any("msg", msg))
		return nil, nil
	}
	return base, msg
}

// 读信息，从 websocket 连接直接读取数据
func (c *WebSocketClient) read() {
	defer c.Close()

	for {
		messageType, message, err := c.Socket.ReadMessage()
		if err != nil || messageType == websocket.CloseMessage {
			break
		}
		// 日志debug级别
		c.log.Sugar.Debugf("client [%d][%s] receive message", c.Uid, c.Id)

		// 处理消息
		startTime := time.Now()
		base, msg := c.Decode(message)
		decodeTick := time.Since(startTime) / time.Nanosecond
		c.log.Log.Debug(
			"decode tick data: ",
			c.log.String("decode", (decodeTick).String()),
		)
		c.log.Sugar.Debugf("recv head:%v, msg:%v", base, msg)
		if base != nil {
			c.Recv(base, msg)
		}
	}
}

func (c *WebSocketClient) process(base *base.BaseMessage, msg interface{}) {
	for _, fun := range c.beforeMiddlewares {
		if !fun(base, msg, c) {
			c.log.Log.Warn("before middleware return false, msg droped", c.log.Any("base", base), c.log.Any("msg", msg))
			base.Cmd += 1
			base.Code = 101
			c.sendPacket(base, nil)
			c.Close()
			return
		}
	}
	startTime := time.Now()
	retCmd, msg, code := c.router.Process(base.Cmd, msg, c)
	processTick := time.Since(startTime) / time.Nanosecond
	c.log.Log.Debug(
		"process tick data: ",
		c.log.String("process", (processTick).String()),
	)

	for _, fun := range c.afterMiddlewares {
		fun(base, msg, c)
	}

	if retCmd == 0 {
		return
	}
	base.Code = code
	base.Cmd = retCmd
	c.sendPacket(base, msg)
}

func (c *WebSocketClient) sendPacket(base *base.BaseMessage, msg interface{}) {
	c.log.Sugar.Debugf("return base:%v, msg:%v", base, msg)
	// 处理返回消息
	startTime := time.Now()
	ret := c.converter.Encode(base, msg)
	encodeTick := time.Since(startTime) / time.Nanosecond
	c.log.Log.Debug(
		"encode tick data: ",
		c.log.String("encode", (encodeTick).String()),
	)
	c.Send(ret)
}

// func (c *WebSocketClient) SendPacket(cmd, code int32, msg interface{}) {
// 	base := &base.BaseMessage{
// 		Cmd:  cmd,
// 		Code: code,
// 	}
// 	c.sendPacket(base, msg)
// }

func (c *WebSocketClient) StartProcess() {
	go c.read()
	go c.processes()
	go c.write()
}

func (c *WebSocketClient) processes() {
	defer c.Close()
	for message := range c.Recvs {
		c.process(message.Base, message.Msg)
	}
}

// 写信息，从 channel 变量 Send 中读取数据写入 websocket 连接
func (c *WebSocketClient) write() {
	defer c.Close()
	for message := range c.Message {
		// debug级别
		c.log.Sugar.Debugf("client [%d][%s] write message", c.Uid, c.Id)
		err := c.Socket.WriteMessage(websocket.BinaryMessage, message)
		if err != nil {
			c.log.Sugar.Infof("client [%s] writemessage err: %s", c.Id, err)
		}
	}
}

// 发送信息
func (c *WebSocketClient) Send(msg []byte) {
	if c.closed {
		c.log.Log.Error("Send client is closed, msg droped", c.log.Any("msg", msg))
		return
	}
	c.Message <- msg
}

func (c *WebSocketClient) Recv(base *base.BaseMessage, msg interface{}) {
	if c.closed {
		c.log.Log.Error("Recv client is closed, msg droped", c.log.Any("msg", msg), c.log.Int32("cmd", base.Cmd))
		return
	}
	c.Recvs <- &ClientMsg{Base: base, Msg: msg}
}

// 设置唯一标识
func (c *WebSocketClient) SetUid(uid uint64) {
	c.Uid = uid
}
